const router = require('express').Router(); 
const { getYear, createYear } = require('../controllers/settings/yearController.js');
const { getCategory, getCategoryById, createCategory, updateCategory, deleteCategory } = require('../controllers/settings/categoryController.js');
const { getProduct, createProduct, updateProduct, deleteProduct } = require('../controllers/products/productController.js');
const { getCity, createCity, updateCity, deleteCity } = require('../controllers/settings/cityController.js');
const { getCountry, createCountry, updateCountry, deleteCountry } = require('../controllers/settings/countryController.js');
const { getBrand, createBrand, updateBrand, deleteBrand } = require('../controllers/settings/brandController.js');
const { getArea, createArea, updateArea, deleteArea } = require('../controllers/settings/areaController.js');
const { getCustomer, createCustomer, updateCustomer, deleteCustomer } = require('../controllers/customer/customerController.js');
const { getOrder, createOrder, updateOrder, deleteOrder } = require('../controllers/orders/orderController.js');
const { getColor, createColor, updateColor, deleteColor } = require('../controllers/settings/colorController.js');
const { getBranch, createBranch, deleteBranch, updateBranch } = require('../controllers/settings/branchController.js');
const { topSaleProduct } = require('../controllers/orders/orderReportController.js');
const { getCoupon, createCoupon, updateCoupon, deleteCoupon } = require('../controllers/coupon/couponController.js');

router.route('/year').get(getYear).post(createYear)
// Category Routes
router.route('/category').get(getCategory).post(createCategory)
router.route('/category/:id').get(getCategoryById).put(updateCategory)

router.route('/brand').get(getBrand).post(createBrand)
router.route('/city').get(getCity).post(createCity)
router.route('/country').get(getCountry).post(createCountry)
router.route('/area').get(getArea).post(createArea)
router.route('/customer').get(getCustomer).post(createCustomer)
router.route('/order').get(getOrder).post(createOrder)
router.route('/topSaleProduct').get(topSaleProduct)

// Product Routes
router.route('/product').get(getProduct).post(createProduct);
router.route('/product/:id').put(updateProduct).delete(deleteProduct);

//coupons Routes
router.route('/coupon').get(getCoupon).post(createCoupon);
router.route('/coupon/:id').put(updateCoupon).delete(deleteCoupon);

module.exports = router;