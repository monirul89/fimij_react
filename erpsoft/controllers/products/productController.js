const ProductInfo = require('../../models/product/productModels.js')
const { ErrorHandler } = require("../../middlewares/error");

module.exports.getProduct = async (req, res) => {
    const products = await ProductInfo.find({}).sort({createdAt: -1})
    res.status(200).json(products)
}

module.exports.getProduct1 = async (req, res) => {
    try{
        const { page, limit } = req.query;
        const pageNum = page ? parseInt(page, 10) : 1;
        const Limit = limit ? parseInt(limit, 10) : 10;
        const skip = Limit * (pageNum - 1);

        if (page) delete req.query.page;
        if (limit) delete req.query.limit;

        const products = await ProductInfo.find(req.query)
            .limit(Limit)
            .skip(skip)
            .sort({createdAt: -1});
            
        const count = await ProductInfo.countDocuments(req.query);
        res.status(200).json({
            status:true,
            data: products,
            count
        })
    }catch(err){
        res.status(500).json(err);
    }
}

module.exports.createProduct = (req, res) => {
    console.log(req.body)
    ProductInfo.insertMany({
        productName: req.body.productName,
        category: req.body.category,
        origin: req.body.origin,
        brand: req.body.brand,
        color: req.body.color,
        description: req.body.description,
        sku: req.body.sku,
        "inventory.purchasedPrice": req.body.purchasedPrice,
        "inventory.salePrice": req.body.salePrice,
        "inventory.discountPrice": req.body.discountPrice,
        "inventory.discountStatus": req.body.discountStatus,
        "inventory.totalQty": req.body.totalQty,
        "inventory.remainQty": req.body.totalQty,
        "inventory.availableQty": req.body.totalQty,
        "inventory.displayQty": req.body.displayQty
    }).then((result) => {
        res.status(200).json(result)
    }).catch((err) => {
        res.status(500).json(err)
    })
}

// module.exports.createProduct = async (req, res) => {
//     const { body } = req;
//     console.log("body: ", req.body)
//     try{
//         const product = await ProductInfo.create(body);
//         res.status(200).json({
//             status:true,
//             data: product
//         });
//     }catch(err){
//         res.status(500).json(err);
//     }
// };

module.exports.updateProduct = async (req, res, next) => {
    const {body, params} = req;
    try{
        const product = await ProductInfo.findOneAndUpdate(
            { _id: params.id },
            body, { new: true });

        res.status(200).json({
            status:true,
            data: product
        })
    }catch(err){
        res.status(500).json(err);
    }
};

module.exports.deleteProduct = async (req, res, next) => {
    try{
        await ProductInfo.findOneAndDelete({ _id: req.params.id });

        res.status(200).json({
            status:true,
            message: 'Product deleted successfully.'
        })
    }catch(err){
        res.status(500).json(err);
    }
};
