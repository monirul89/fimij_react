const bcrypt = require('bcrypt')
const CustomerInfo = require('../../models/customer/customerModels.js')

module.exports.getCustomer = (req, res) => {
    CustomerInfo.find().sort({createdAt: -1}).then((result) => {
        res.status(200).json(result)
    }).catch((err) => {
        res.status(500).json(err)
    })
}

module.exports.createCustomer = async (req, res) => {
    try{
        const hashedPassword = await bcrypt.hash(req.body.password, 10)
        
        const customer = await CustomerInfo.create({ 
            name: req.body.name,
            mobile: req.body.mobile,
            email: req.body.email,
            password: hashedPassword,
            city: req.body.city,
            country: req.body.country,
            zipcode: req.body.zipcode,
            area: req.body.area,
            customerType: req.body.customerType,
            images: req.body.images,
        })

        if(customer){
            res.status(200).json(customer)
        }
        else{
            res.status(500).json("Customer does not created!")
        }
    }
    catch {
        res.status(500).json(err)
    }
}

module.exports.updateCustomer = (req, res) => {

}

module.exports.deleteCustomer = (req, res) => {

}