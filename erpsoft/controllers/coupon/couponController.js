const CouponInfo = require('../../models/coupon/couponModels')
const { ErrorHandler } = require("../../middlewares/error");

module.exports.getCoupon = async (req, res) => {

    try{
        const coupons = await CouponInfo.find({})
            .populate("customer", ["name","mobile","email","customerType","images"])
            .sort({createdAt: -1})
        if(coupons){
            res.status(200).json(coupons)
        }
        else{
            res.status(500).json(err)
        }
    }
    catch(err){
        res.status(500).json(err);
    }
}


module.exports.createCoupon = (req, res) => {
   
    CouponInfo.insertMany({
        name: req.body.name,
        code: req.body.code,
        isPercent: req.body.isPercent,
        value: req.body.value,
        minimumSpend: req.body.minimumSpend,
        maximumSpend: req.body.maximumSpend,
        usageLimitPerCoupon: req.body.usageLimitPerCoupon,
        usageLimitPerCustomer: req.body.usageLimitPerCustomer,
        isActive: req.body.isActive,
        customer: req.body.customer,


    }).then((result) => {
        res.status(200).json(result)
    }).catch((err) => {
        res.status(500).json(err)
    })
}


module.exports.updateCoupon = async (req, res, next) => {
    const {body, params} = req;
    try{
        const coupon = await CouponInfo.findOneAndUpdate(
            { _id: params.id },
            body, { new: true });

        res.status(200).json({
            status:true,
            data: coupon
        })
    }catch(err){
        res.status(500).json(err);
    }
};

module.exports.deleteCoupon = async (req, res, next) => {
    try{
        await CouponInfo.findOneAndDelete({ _id: req.params.id });

        res.status(200).json({
            status:true,
            message: 'Coupon deleted successfully.'
        })
    }catch(err){
        res.status(500).json(err);
    }
};
