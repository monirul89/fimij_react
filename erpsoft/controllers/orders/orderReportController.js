const OrderInfo = require('../../models/orders/orderModels.js')
const ProductInfo = require('../../models/product/productModels.js')

module.exports.topSaleProduct = (req, res) => {
    OrderInfo.aggregate([
        {
            $match: { "orderDetails.orderQty": { $gte: 1 } }
        },
        {
            $group: {_id: "$customer" }
        },
        {
            $project: { orderNumber: 1, maskingNumber: 1, totalAmount: 1}
        }
    ]) 
    .then((result) => {
        console.log("result: ", result)
        res.status(200).json({ message: result })
    })
    .catch((err) => {
        res.status(500).json({ message: err})
    })
}