const OrderInfo = require('../../models/orders/orderModels.js')

module.exports.getOrder = (req, res) => {
    OrderInfo.find({deleted: false})
    .populate("customer", ["name","mobile","email","customerType","images"])
    .populate("orderDetails.product", ["productName","category","sku","productImg"])
    .sort({createdAt: -1})
    .then((result) => {
        res.status(200).json(result)
    }).catch((err) => {
        res.status(500).json(err)
    })
}

module.exports.createOrder = async (req, res) => {
    try {
        let totayISODate = new Date().toISOString().split("T")[0]+"T00:00:00Z";
        let todayTotalOrder = await OrderInfo.find({createdAt: { $gte: totayISODate }, deleted: false}, {maskingNumber: 0}).count()
    
        if(todayTotalOrder == 0) { todayTotalOrder = 1 }
        else{ todayTotalOrder += 1 }
    
        let newMaskingNumber = totayISODate.slice(0,4).toString()+totayISODate.slice(5,7).toString()+totayISODate.slice(8,10).toString()+todayTotalOrder.toString()
    
        OrderInfo.insertMany({
            orderNumber: "SLO#"+newMaskingNumber.toString(),
            orderStatus: req.body.orderStatus,
            maskingNumber: newMaskingNumber,
            customer: req.body.customer,
            orderDetails: req.body.orderDetails,
            shippingCost: req.body.shippingCost,
            discountAmount: req.body.discountAmount,
            vatAmount: req.body.vatAmount,
            grandTotal: req.body.grandTotal,
            remainAmount: req.body.remainAmount,
            deliveryTargetDate: req.body.deliveryTargetDate,
            shippingAddress: req.body.shippingAddress,
            paymentTypes: req.body.paymentTypes
        }).then((result) => {
            OrderInfo.updateMany({ _id: result[0]["_id"] },
                { $set: { orderDetails: req.body.orderDetails } }).then((data) => {
                    res.status(200).json(result)
                }).catch((err) => {
                    res.status(500).json(err)
                }
            )
        }).catch((err) => {
            res.status(500).json(err)
        })
    }
    catch {
        res.status(500).json({message: "Sales order creation error"})
    }
}

module.exports.updateOrder = (req, res) => {

}

module.exports.deleteOrder = (req, res) => {

}