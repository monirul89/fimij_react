const ColorInfo = require('../../models/settings/colorModels.js')

module.exports.getColor = (req, res) => {
    ColorInfo.find({deleted: false}, {name: 1, code: 1}).then((result) => {
        res.status(200).json({message: result})
    }).catch((err) => {
        res.status(500).json(err)
    })
}

module.exports.createColor = async (req, res) => {
    try{
        const color = await ColorInfo.insertMany({
            name: req.body.name,
            code: req.body.code
        })

        res.status(200).json({message: color})
    }catch (err){
        res.status(500).json(err)
    }
}

module.exports.updateColor = async (req, res) => {
    try{
        const color = await ColorInfo.findByIdAndUpdate(req.params.id, {$set: {name: req.body.name} })
        if(color){
            res.status(200).json({message: color});
        }
        else{
            res.status(200).json({message: "Color updated failed!"});
        }
    }catch (err){
        res.status(500).json(err)
    }
}

module.exports.deleteColor = (req, res) => {
    ColorInfo.findByIdAndDelete(req.params.id).then((result) => {
        res.status(200).json(result)
    }).catch((err) => {
        res.status(500).json(err)
    })
}