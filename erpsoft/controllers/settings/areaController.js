const AreaInfo = require('../../models/settings/areaModels.js')

module.exports.getArea = (req, res) => {
    AreaInfo.find().then((result) => {
        res.status(200).json(result)
    }).catch((err) => {
        res.status(500).json(err)
    })
}

module.exports.createArea = (req, res) => {
    AreaInfo.insertMany({ areaName: req.body.areaName }).then((result) => {
        res.status(200).json(result)
    }).catch((err) => {
        res.status(500).json(err)
    })
}

module.exports.updateArea = (req, res) => {

}

module.exports.deleteArea = (req, res) => {

}