const CategoryInfo = require('../../models/settings/categoryModels.js')

module.exports.getCategory = async (req, res) => {
    try{
        const { page, limit } = req.query;
        const pageNum = page ? parseInt(page, 10) : 1;
        const Limit = limit ? parseInt(limit, 10) : 10;
        const skip = Limit * (pageNum - 1);

        if (page) delete req.query.page;
        if (limit) delete req.query.limit
        req.query.deleted = false;

        const categories = await CategoryInfo.find(req.query)
            .limit(Limit)
            .skip(skip)
            .sort({createdAt: -1});

        const count = await CategoryInfo.countDocuments(req.query);
        res.status(200).json({
            status:true,
            data: categories,
            count
        })
    }catch(err){
        res.status(500).json(err);
    }
}

module.exports.getCategoryById = async (req, res) => {
    try{
        const category = await CategoryInfo.findOne({_id: req.params.id, deleted: false});

        res.status(200).json({
            status:true,
            data: category,
        })
    }catch(err){
        res.status(500).json(err);
    }
}

module.exports.createCategory = (req, res) => {
    CategoryInfo.insertMany({ catName: req.body.catName, status: req.body.status }).then((result) => {
        res.status(200).json(result)
    }).catch((err) => {
        res.status(500).json(err) 
    })
}

module.exports.updateCategory = async (req, res, next) => {
    const {body, params} = req;
    try{
        const category = await CategoryInfo.findOneAndUpdate(
            { _id: params.id },
            body, { new: true });

        res.status(200).json({
            status:true,
            data: category
        })
    }catch(err){
        res.status(500).json(err);
    }
};

module.exports.deleteCategory = (req, res) => {
    console.log(req.body)
    CategoryInfo.findById(req.body.id).then((result) => {
        res.status(200).json(result)
    }).catch((err) => {
        res.status(500).json(err) 
    })
}