const YearsInfo = require('../../models/settings/yearModels.js')

module.exports.getYear = (req, res) => {
    YearsInfo.find().then((result) => {
        res.status(200).json(result)
    }).catch((err) => {
        res.status(500).json(err)
    })
}

module.exports.createYear = (req, res) => {
    console.log("Create Year", req)
    YearsInfo.insertMany({ year: req.body.year }).then((result) => {
        res.status(200).json(result)
    }).catch((err) => {
        res.status(500).json(err)
    })
}

module.exports.updateYear = (req, res) => {

}

module.exports.deleteYear = (req, res) => {

}