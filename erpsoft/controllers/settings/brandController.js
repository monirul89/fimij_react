const BrandInfo = require('../../models/settings/brandModels.js')

module.exports.getBrand = (req, res) => {
    BrandInfo.find().then((result) => {
        res.status(200).json(result)
    }).catch((err) => {
        res.status(500).json(err)
    })
}

module.exports.createBrand = (req, res) => {
    BrandInfo.insertMany({ brandName: req.body.brandName, status: req.body.status }).then((result) => {
        res.status(200).json(result)
    }).catch((err) => {
        res.status(500).json(err)
    })
}

module.exports.updateBrand = (req, res) => {

}

module.exports.deleteBrand = (req, res) => {

}