const BranchInfo = require('../../models/settings/branchModels.js')

module.exports.getBranch = (req, res) => {
    BranchInfo.find({}).then((result) =>{
        res.status(200).json({message: result})
    }).catch((err) => {
        res.status(500).json({message: err})
    })
}

module.exports.createBranch = async (req, res) => {
    try{
        const branch = await BranchInfo.insertMany(
            {
                branchName: req.body.branchName,
                branch_id: req.body.branch_id
            }
        )
        res.status(200).json({message: branch})
    }
    catch(err){
        res.status(500).json({message: err})
    }
}
module.exports.updateBranch = async (req, res) => {
    try
    {
        const branch = await BranchInfo.findByIdAndUpdate(req.params.id, {$set:{branch_name: req.body.branch_name}})
        if (branch){
            res.status(200).json({message: branch})
        }
        else{
            res.status(200).json({message: "Branch Not Updated!"})
        }
    }
    catch(err){
        res.status(500).json({message: err})
    }
}

module.exports.deleteBranch = (req, res) => {
    BranchInfo.findByIdAndDelete(req.params.id).then((result) =>{
        res.status(200).json({message: result})
    }).catch((err) => {
        res.status(500).json({message: err})
    })
}