const CountryInfo = require('../../models/settings/countryModels.js')

module.exports.getCountry = (req, res) => {
    CountryInfo.find().sort({countryName: 1}).then((result) => {
        res.status(200).json(result)
    }).catch((err) => {
        res.status(500).json(err)
    })
}

module.exports.createCountry = (req, res) => {
    CountryInfo.insertMany({ countryName: req.body.countryName, code: req.body.code, status: req.body.status }).then((result) => {
        res.status(200).json(result)
    }).catch((err) => {
        res.status(500).json(err)
    })
}

module.exports.updateCountry = (req, res) => {

}

module.exports.deleteCountry = (req, res) => {

}