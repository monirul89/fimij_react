const CityInfo = require('../../models/settings/cityModels.js')

module.exports.getCity = (req, res) => {
    CityInfo.find().then((result) => {
        res.status(200).json(result)
    }).catch((err) => {
        res.status(500).json(err)
    })
}

module.exports.createCity = (req, res) => {
    console.log("req")
    CityInfo.insertMany({ cityName: req.body.cityName }).then((result) => {
        res.status(200).json(result)
    }).catch((err) => {
        res.status(500).json(err)
    })
}

module.exports.updateCity = (req, res) => {

}

module.exports.deleteCity = (req, res) => {

}