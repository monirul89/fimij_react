const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({
    orderNumber: {
        type: String,
        unique: true,
        index: true,
        required: true
    },
    maskingNumber: {
        type: Number,
        unique: true,
        index: true,
        required: true
    },
    orderStatus: {
        type: String,
        enum: ['Pending','Processing','Delivered','Canceled','Returned'],
        default: "Pending"
    },
    customer: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Customer'
    },
    orderDetails: [{
        product: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Product'
        },
        orderQty: {
            type: Number,
            default: 0,
        },
        unitPrice: {
            type: Number,
            default: 0
        },
        totalPrice: {
            type: Number,
            default: 0
        },
        productStatus: {
            type: String,
            enum: ['Pending','Processing','Delivered','Canceled','Returned'],
            default: "Pending"
        }
    }],
    paymentDetails: [{
        paymentTypes: {
            type: String,
            enum: ['Cash','Card','Mobile Banking','Cheque','Gift Card','Point','Paypal','Others'],
            trim: true
        },
        bankName: {
            type: String,
            trim: true
        },
        paidAmount: {
            type: Number,
            default: 0
        },
        transactionRef: {
            type: Number,
            default: 0
        },
        createdAt: {
            type: Date
        },
        updatedAt: {
            type: Date
        },
        deleted: {
            type: Boolean,
            default: false
        }
    }],
    shippingCost: {
        type: Number,
        default: 0
    },
    discountAmount: {
        type: Number,
        default: 0
    },
    vatAmount: {
        type: Number,
        default: 0
    },
    grandTotal: {
        type: Number,
        default: 0
    },
    remainAmount: {
        type: Number,
        default: 0
    },
    refundStatus: {
        type: Boolean,
        default: false
    },
    usedPoints: {
        type: Number,
        default: 0
    },
    paymentTypes: [],
    deliveryTargetDate: {
        type: Date
    },
    deliveryDate: {
        type: Date
    },
    shippingAddress: {
        type: String,
        required: true,
        trim: true
    },
    deleted: {
        type: Boolean,
        default: false
    }
}, {timestamps: true})

module.exports = mongoose.model("Order", orderSchema)