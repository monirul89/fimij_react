const mongoose = require('mongoose');

const areaSchema = new mongoose.Schema({
    areaName: {
        type: String,
        unique: true,
        required: true,
        index: true,
        trim: true
    },
    status: {
        type: Boolean,
        default: true
    },
    deleted: {
        type: Boolean,
        default: false
    }
}, {timestamps: true})

module.exports = mongoose.model('Area', areaSchema)