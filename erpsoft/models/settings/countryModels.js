const mongoose = require('mongoose');

const countrySchema = new mongoose.Schema({
    countryName: {
        type: String,
        unique: true,
        required: true,
        index: true,
        trim: true
    },
    code: {
        type: String,
        unique: true,
        trim: true
    },
    status: {
        type: Boolean,
        default: true
    },
    deleted: {
        type: Boolean,
        default: false
    }
}, {timestamps: true})

module.exports = mongoose.model('Country', countrySchema)