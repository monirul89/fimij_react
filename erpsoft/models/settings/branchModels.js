const mongoose = require('mongoose');

const branchSceema = mongoose.Schema({
    branchName: {
        type: String,
        required: true,
        length: 50,
        unique: true,
        index: true,
        trim: true
    },
    branchID: {
        type: String,
        required: true,
        length: 20,
        unique: true,
        index: true,
        trim: true
    },
    deleted: {
        type: Boolean,
        default: false
    }
}, {timestamp:true})

module.exports = mongoose.model('Branch', branchSceema)