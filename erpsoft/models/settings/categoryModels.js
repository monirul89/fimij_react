const mongoose = require('mongoose');

const categorySchema = new mongoose.Schema({
    catName: {
        type: String,
        required: true,
        unique: true,
        index: true,
        trim: true
    },
    status: {
        type: Boolean,
        default: true
    },
    deleted: {
        type: Boolean,
        default: false
    }
}, {timestamps: true})

module.exports = mongoose.model('Category', categorySchema)