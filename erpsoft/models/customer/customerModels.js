const mongoose = require('mongoose');

const customerSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        index: true,
        trim: true
    },
    mobile:{
        type: String,
        required: true,
        unique: true,
        index: true,
        trim: true
    },
    email: {
        type: String,
        index: true,
        trim: true
    },
    password: {
        type: String,
        required: true,
        trim: true
    },
    city: {
        type: String,
        index: true,
        trim: true
    },
    country: {
        type: String,
        trim: true
    },
    zipcode: {
        type: Number
    },
    area: {
        type: String,
        trim: true
    },
    images: [],
    customerType: {
        type: String,
        required: true,
        enum: ['Normal', 'Gold','Diamond'],
        default: "Normal"
    },
    totalPoints: {
        type: Number,
        default: 0
    },
    availablePoints: {
        type: Number,
        default: 0
    },
    usedPoints: {
        type: Number,
        default: 0
    },
    createdIPAddress: {
        type: String,
        trim: true
    },
    deleted: {
        type: Boolean,
        default: false
    },
    status: {
        type: Boolean,
        default: true
    }
}, {timestamps: true})

module.exports = mongoose.model("Customer", customerSchema)