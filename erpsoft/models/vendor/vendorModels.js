const mongoose = require('mongoose');

const vendorSchema = new mongoose.Schema({
    vendorName: {
        type: String,
        index: true,
        required: true,
        trim: true
    },
    vendorImg: [],
    vendorMobile: {
        type: String,
        index: true,
        trim: true
    },
    vendorEmail: {
        type: String,
        index: true,
        trim: true
    },
    businessType: {
        type: String,
        trim: true
    },
    paymentMethod: [],
    tradeLicenceNo: {
        type: String,
        trim: true
    },
    tin: {
        type: String,
        trim: true
    },
    contactPerson: {
        type: String,
        index: true,
        trim: true
    },
    contactPersonMobile: {
        type: String,
        trim: true
    },
    contactPersonEmail: {
        type: String,
        trim: true
    },
    vendorAddress: {
        type: String,
        trim: true
    },
    country: {
        type: String,
        trim: true
    },
    website: {
        type: String,
        trim: true
    },
    vendorRating: {
        type: Number,
        default: 0
    },
    status: {
        type: String,
        enum: ['Active','Inactive','Pending','Rejected','Blocked'],
        default: 'Pending'
    },
    deleted: {
        type: Boolean,
        default: false
    }
}, {timestamps: true})

module.exports = mongoose.model('Vendor', vendorSchema)