const mongoose = require('mongoose');

const couponSchema = new mongoose.Schema({
     name: {
        type: String,
        required: true,
        index: true,
        trim: true
    },
    code: {
        type: String,
        required: true,
        unique: true,
        index: true,
        trim: true
    },
    isPercent: {
        type: Boolean,
        default: false
    },
    value: {
        type: Number,
        default: 0,
    },
    customer: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Customer'
    },
    minimumSpend: {
        type: Number,
        default: 0,
    },
    maximumSpend: {
        type: Number,
        default: 0,
    },
    usageLimitPerCoupon: {
        type: Number,
        default: 0,
    },
    usageLimitPerCustomer: {
        type: Number,
        default: 0,
    },
    isActive: {
        type: Boolean,
        default: true,
    },
    deleted: {
        type: Boolean,
        default: false
    }
}, {timestamps: true})

module.exports = mongoose.model('Coupon', couponSchema)