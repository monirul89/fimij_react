const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({
    productName: {
        type: String,
        required: true,
        unique: true,
        index: true,
        trim: true
    },
    category: {
        type: String,
        required: true,
        trim: true
    },
    origin: {
        type: String,
        trim: true,
        default: "Third Party"
    },
    brand: {
        type: String,
        trim: true
    },
    color: [],
    description: {
        type: String,
        trim: true
    },
    note: {
        type: String,
        trim: true
    },
    isMart: {
        type: Boolean,
        default: false
    },
    sku: {
        type: String,
        trim: true
    },
    inventory: {
        purchasedPrice: {
            type: Number,
            default: 0
        },
        salePrice: {
            type: Number,
            default: 0
        },
        discountPrice: {
            type: Number,
            default: 0
        },
        discountDateFrom: {
            type: Date
        },
        discountDateTo: {
            type: Date
        },
        discountStatus: {
            type: Boolean,
            default: false
        },
        totalQty: {
            type: Number,
            default: 0
        },
        remainQty: {
            type: Number,
            default: 0
        },
        displayQty: {
            type: Number,
            default: 0
        },
        totalSaleQty: {
            type: Number,
            default: 0
        },
        stockStatus: {
            type: String,
            default: "Abailable"
        }
    },
    productImg: [],
    history: {
        totalView: {
            type: Number,
            default: 0
        },
        totalLike: {
            type: Number,
            default: 0
        },
        comments: [{
            customerName: {
                type: String,
                trim: true
            },
            customerEmail: {
                type: String,
                trim: true
            },
            rating: {
                type: Number,
                default: 1
            },
            comment: {
                type: String,
                trim: true
            },
            createdAt: {
                type: Date
            },
            ipAddrss: {
                type: String,
                trim: true
            }
        }]
    },
    seo: {
        title: {
            type: String,
            trim: true
        },
        metaDescription: {
            type: String,
            trim: true
        },
        ogImage: {
            type: String,
            trim: true
        },
        ogTitle: {
            type: String,
            trim: true
        },
        ogType: {
            type: String,
            trim: true
        },
        ogUrl: {
            type: String,
            trim: true
        }
    },
    youtubeVideoLink: [{
        type: String 
    }],
    deleted: {
        type: Boolean,
        default: false
    }
}, {timestamps: true})

module.exports = mongoose.model('Product', productSchema)