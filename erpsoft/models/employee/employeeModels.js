const mongoose = require('mongoose');

const employeeSchema = new mongoose.Schema({
    firstName: {
        type: String,
        index: true,
        required: true,
        trim: true
    },
    lastName: {
        type: String,
        index: true,
        trim: true
    },
    mobile: {
        type: String,
        index: true,
        required: true,
        unique: true,
        trim: true
    },
    email: {
        type: String,
        index: true,
        unique: true,
        trim: true
    },
    password: {
        type: String,
        trim: true
    },
    employeeType: {
        type: String,
        trim: true
    },
    fatherName: {
        type: String,
        required: true,
        trim: true
    },
    motherName: {
        type: String,
        required: true,
        trim: true
    },
    gender: {
        type: String,
        required: true,
        trim: true
    },
    religion: {
        type: String,
        required: true,
        trim: true
    },
    dob: {
        type: Date
    },
    nid: {
        type: String,
        trim: true
    },
    nationality: {
        type: String,
        required: true,
        default: 'Bangladeshi',
        trim: true
    },
    images: [],
    status: {
        type: String,
        enum: ['Active','Inactive','Blocked'],
        trim: true
    }

})

module.exports = mongoose.model('Employee', employeeSchema)