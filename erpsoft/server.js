const express = require('express');
const app = express();
const mongoose = require('mongoose');
const cors = require('cors');
const dotenv = require('dotenv')
const path = require('path');
const http = require('http');
const expressServer = http.createServer(app);

const { Server } = require('socket.io');
const io = new Server(expressServer);

const adminRoutes = require('./routes/adminRoutes.js');

dotenv.config();
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(express.raw());
app.use(cors());

app.set('view engine', 'ejs'); 
app.use(express.static('client/build'));

mongoose.connect(`${process.env.DB_NAME}`, { useNewUrlParser: true, useUnifiedTopology: true })
.then(() => {
    expressServer.listen(process.env.PORT, () => {
        console.log(`Server is running on port http://127.0.0.1:${process.env.PORT}`)
    })
}).catch(err => {
    console.log(err);
});

// app.get('*', (req, res) => {
//     console.log("Request from react: ", req)
//     res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'))
// })

app.use('/admin/', adminRoutes);

module.exports = app;