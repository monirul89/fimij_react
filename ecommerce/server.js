const express = require("express");
const app = express();
const path = require('path');

const webpackDevMiddleware = require('webpack-dev-middleware');
const webpackHotMiddleware = require('webpack-hot-middleware');
const webpack = require('webpack');
const config = require('./webpack.config');
const compiler = webpack(config);
app.use(webpackDevMiddleware(compiler, { publicPath: config.output.publicPath }));
app.use(webpackHotMiddleware(compiler));


app.use(express.static(path.join(__dirname,'./public')));

///add this line of code to server.js
 app.get("/*",(req,res)=>{
    res.sendFile(path.join(__dirname,"./dist/index.html"))
})

///add this line of code to server.js
 app.get("/",(req,res)=>{
    res.sendFile(path.join(__dirname,"./dist/index.html"))
})

app.listen(4000,()=>console.info(`server is running at http://localhost:4000`))