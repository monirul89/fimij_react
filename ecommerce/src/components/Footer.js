import React from "react";


const Footer = () => {
	return (
		<div>
			{/* <!-- //newsletter -->*/}
			<div className="footer-top">
				<div className="container-fluid">
					<div className="col-xs-8 agile-leftmk">
						<h2>Get your Groceries delivered from local stores</h2>
						<p>Free Delivery on your first order!</p>
						<form action="#" method="post">
							<input type="email" placeholder="E-mail" name="email" required="" />
							<input type="submit" value="Subscribe" />
						</form>
						<div className="newsform-w3l">
							<span className="fa fa-envelope-o" aria-hidden="true"></span>
						</div>
					</div>
					<div className="col-xs-4 w3l-rightmk">
						<img src="/assets/images/tab3.png" alt=" " />
					</div>
					<div className="clearfix"></div>
				</div>
			</div>
			{/* <!-- footer --> */}
			<footer>
				<div className="container">
					{/* <!-- footer third section -->  */}
					<div className="footer-info w3-agileits-info">
						{/* <!-- footer categories --> */}
						<div className="col-sm-5 address-right">
							<div className="col-xs-6 footer-grids">
								<h3>Categories</h3>
								<ul>
									<li>
										<a href="product.html">Grocery</a>
									</li>
									<li>
										<a href="product.html">Fruits</a>
									</li>
									<li>
										<a href="product.html">Soft Drinks</a>
									</li>
									<li>
										<a href="product2.html">Dishwashers</a>
									</li>
									<li>
										<a href="product.html">Biscuits & Cookies</a>
									</li>
									<li>
										<a href="product2.html">Baby Diapers</a>
									</li>
								</ul>
							</div>
							<div className="col-xs-6 footer-grids agile-secomk">
								<ul>
									<li>
										<a href="product.html">Snacks & Beverages</a>
									</li>
									<li>
										<a href="product.html">Bread & Bakery</a>
									</li>
									<li>
										<a href="product.html">Sweets</a>
									</li>
									<li>
										<a href="product.html">Chocolates & Biscuits</a>
									</li>
									<li>
										<a href="product2.html">Personal Care</a>
									</li>
									<li>
										<a href="product.html">Dried Fruits & Nuts</a>
									</li>
								</ul>
							</div>
							<div className="clearfix"></div>
						</div>
						{/* <!-- //footer categories -->
						<!-- quick links --> */}
						<div className="col-sm-5 address-right">
							<div className="col-xs-6 footer-grids">
								<h3>Quick Links</h3>
								<ul>
									<li>
										<a href="about.html">About Us</a>
									</li>
									<li>
										<a href="contact.html">Contact Us</a>
									</li>
									<li>
										<a href="help.html">Help</a>
									</li>
									<li>
										<a href="faqs.html">Faqs</a>
									</li>
									<li>
										<a href="terms.html">Terms of use</a>
									</li>
									<li>
										<a href="privacy.html">Privacy Policy</a>
									</li>
								</ul>
							</div>
							<div className="col-xs-6 footer-grids">
								<h3>Get in Touch</h3>
								<ul>
									<li>
										<i className="fa fa-map-marker"></i> 123 Sebastian, USA.</li>
									<li>
										<i className="fa fa-mobile"></i> 333 222 3333 </li>
									<li>
										<i className="fa fa-phone"></i> +222 11 4444 </li>
									<li>
										<i className="fa fa-envelope-o"></i>
										<a href="mailto:example@mail.com"> mail@example.com</a>
									</li>
								</ul>
							</div>
						</div>
						{/* <!-- //quick links -->
						<!-- social icons --> */}
						<div className="col-sm-2 footer-grids  w3l-socialmk">
							<h3>Follow Us on</h3>
							<div className="social">
								<ul>
									<li>
										<a className="icon fb" href="#">
											<i className="fa fa-facebook"></i>
										</a>
									</li>
									<li>
										<a className="icon tw" href="#">
											<i className="fa fa-twitter"></i>
										</a>
									</li>
									<li>
										<a className="icon gp" href="#">
											<i className="fa fa-google-plus"></i>
										</a>
									</li>
								</ul>
							</div>
							<div className="agileits_app-devices">
								<h5>Download the App</h5>
								<a href="#">
									<img src="/assets/images/1.png" alt="" />
								</a>
								<a href="#">
									<img src="/assets/images/2.png" alt="" />
								</a>
								<div className="clearfix"> </div>
							</div>
						</div>
						{/* <!-- //social icons --> */}
						<div className="clearfix"></div>
					</div>
					{/* <!-- //footer third section -->
					<!-- footer fourth section (text) --> */}
					<div className="agile-sometext">
						{/* <!-- payment --> */}
						<div className="sub-some child-momu">
							<h5>Payment Method</h5>
							<ul>
								<li>
									<img src="/assets/images/pay2.png" alt="" />
								</li>
								<li>
									<img src="/assets/images/pay5.png" alt="" />
								</li>
								<li>
									<img src="/assets/images/pay1.png" alt="" />
								</li>
								<li>
									<img src="/assets/images/pay4.png" alt="" />
								</li>
								<li>
									<img src="/assets/images/pay6.png" alt="" />
								</li>
								<li>
									<img src="/assets/images/pay3.png" alt="" />
								</li>
								<li>
									<img src="/assets/images/pay7.png" alt="" />
								</li>
								<li>
									<img src="/assets/images/pay8.png" alt="" />
								</li>
								<li>
									<img src="/assets/images/pay9.png" alt="" />
								</li>
							</ul>
						</div>
					</div>
				</div>
			</footer>
			{/* <!-- copyright -->  */}
			<div className="copy-right">
				<div className="container">
					<p>© 2017 Grocery Shoppy. All rights reserved | Design by
						<a href="http://w3layouts.com"> W3layouts.</a>
					</p>
				</div>
			</div>
		</div>
	)
}

export default Footer;