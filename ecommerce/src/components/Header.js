import React from "react";
import HeaderTop from "./HeaderTop.js";
import Navbar from "./Navbar.js";

const Header = () => {
	return (
		<div>
			<HeaderTop/>
			<Navbar />
		
		</div>
	)
}

export default Header;