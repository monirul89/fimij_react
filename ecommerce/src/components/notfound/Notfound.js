
import React, { Component } from "react";
import { Link } from "react-router-dom";

class Notfound extends Component{
	render(){
		return  <div className="main-content">
					<div className="page-content">
						<div className="container-fluid">
							<h1>Not found 404</h1>
						</div>
					</div>
				</div>
	}
}

export default Notfound;