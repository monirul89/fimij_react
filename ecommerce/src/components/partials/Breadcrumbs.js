import React from "react";

const Breadcrumbs = ()=>{
    return <div class="services-breadcrumb">
                <div class="agile_inner_breadcrumb">
                    <div class="container">
                        <ul class="w3_short">
                            <li>
                                <a href="index.html">Home</a>
                                <i>|</i>
                            </li>
                            <li>Single Page</li>
                        </ul>
                    </div>
                </div>
            </div>
}

export default Breadcrumbs;