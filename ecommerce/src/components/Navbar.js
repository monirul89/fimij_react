import React from "react";
import { Link } from "react-router-dom";

const Navbar = () => {
	return (
			<div className="ban-top">  {/*navbar*/}
				<div className="container">
					<div className="agileits-navi_search">
						<form action="#" method="post">
							<select id="agileinfo-nav_search" name="agileinfo_search" required="">
								<option value="">All Categories</option>
								<option value="Kitchen">Kitchen</option>
								<option value="Household">Household</option>
								<option value="Snacks &amp; Beverages">Snacks & Beverages</option>
								<option value="Personal Care">Personal Care</option>
								<option value="Gift Hampers">Gift Hampers</option>
								<option value="Fruits &amp; Vegetables">Fruits & Vegetables</option>
								<option value="Baby Care">Baby Care</option>
								<option value="Soft Drinks &amp; Juices">Soft Drinks & Juices</option>
								<option value="Frozen Food">Frozen Food</option>
								<option value="Bread &amp; Bakery">Bread & Bakery</option>
								<option value="Sweets">Sweets</option>
							</select>
						</form>
					</div>
					<div className="top_nav_left">
						<nav className="navbar navbar-default">
							<div className="container-fluid">
								<div className="navbar-header">
									<button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"
										aria-expanded="false">
										<span className="sr-only">Toggle navigation</span>
										<span className="icon-bar"></span>
										<span className="icon-bar"></span>
										<span className="icon-bar"></span>
									</button>
								</div>
								{/* <!-- Collect the nav links, forms, and other content for toggling --> */}
								<div className="collapse navbar-collapse menu--shylock" id="bs-example-navbar-collapse-1">
									<ul className="nav navbar-nav menu__list">
										<li className="active">
											<Link className="nav-stylehead" to="/">Home
												<span className="sr-only">(current)</span>
											</Link>
										</li>
										<li className="">
											<a className="nav-stylehead" href="about.html">About Us</a>
										</li>
										<li className="dropdown">
											<a href="#" className="dropdown-toggle nav-stylehead" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Kitchen
												<span className="caret"></span>
											</a>
											<ul className="dropdown-menu multi-column columns-3">
												<div className="agile_inner_drop_nav_info">
													<div className="col-sm-4 multi-gd-img">
														<ul className="multi-column-dropdown">
															<li>
																<a href="product.html">Bakery</a>
															</li>
															<li>
																<a href="product.html">Baking Supplies</a>
															</li>
															<li>
																<a href="product.html">Coffee, Tea & Beverages</a>
															</li>
															<li>
																<a href="product.html">Dried Fruits, Nuts</a>
															</li>
															<li>
																<a href="product.html">Sweets, Chocolate</a>
															</li>
															<li>
																<a href="product.html">Spices & Masalas</a>
															</li>
															<li>
																<a href="product.html">Jams, Honey & Spreads</a>
															</li>
														</ul>
													</div>
													<div className="col-sm-4 multi-gd-img">
														<ul className="multi-column-dropdown">
															<li>
																<a href="product.html">Pickles</a>
															</li>
															<li>
																<a href="product.html">Pasta & Noodles</a>
															</li>
															<li>
																<a href="product.html">Rice, Flour & Pulses</a>
															</li>
															<li>
																<a href="product.html">Sauces & Cooking Pastes</a>
															</li>
															<li>
																<a href="product.html">Snack Foods</a>
															</li>
															<li>
																<a href="product.html">Oils, Vinegars</a>
															</li>
															<li>
																<a href="product.html">Meat, Poultry & Seafood</a>
															</li>
														</ul>
													</div>
													<div className="col-sm-4 multi-gd-img">
														<img src="/assets/images/nav.png" alt="" />
													</div>
													<div className="clearfix"></div>
												</div>
											</ul>
										</li>
										<li className="dropdown">
											<a href="#" className="dropdown-toggle nav-stylehead" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Household
												<span className="caret"></span>
											</a>
											<ul className="dropdown-menu multi-column columns-3">
												<div className="agile_inner_drop_nav_info">
													<div className="col-sm-6 multi-gd-img">
														<ul className="multi-column-dropdown">
															<li>
																<a href="product2.html">Kitchen & Dining</a>
															</li>
															<li>
																<a href="product2.html">Detergents</a>
															</li>
															<li>
																<a href="product2.html">Utensil Cleaners</a>
															</li>
															<li>
																<a href="product2.html">Floor & Other Cleaners</a>
															</li>
															<li>
																<a href="product2.html">Disposables, Garbage Bag</a>
															</li>
															<li>
																<a href="product2.html">Repellents & Fresheners</a>
															</li>
															<li>
																<a href="product2.html"> Dishwash</a>
															</li>
														</ul>
													</div>
													<div className="col-sm-6 multi-gd-img">
														<ul className="multi-column-dropdown">
															<li>
																<a href="product2.html">Pet Care</a>
															</li>
															<li>
																<a href="product2.html">Cleaning Accessories</a>
															</li>
															<li>
																<a href="product2.html">Pooja Needs</a>
															</li>
															<li>
																<a href="product2.html">Crackers</a>
															</li>
															<li>
																<a href="product2.html">Festive Decoratives</a>
															</li>
															<li>
																<a href="product2.html">Plasticware</a>
															</li>
															<li>
																<a href="product2.html">Home Care</a>
															</li>
														</ul>
													</div>
													<div className="clearfix"></div>
												</div>
											</ul>
										</li>
										<li className="">
											<a className="nav-stylehead" href="faqs.html">Faqs</a>
										</li>
										<li className="dropdown">
											<a className="nav-stylehead dropdown-toggle" href="#" data-toggle="dropdown">Pages
												<b className="caret"></b>
											</a>
											<ul className="dropdown-menu agile_short_dropdown">
												<li>
													<a href="icons.html">Web Icons</a>
												</li>
												<li>
													<a href="typography.html">Typography</a>
												</li>
											</ul>
										</li>
										<li className="">
											<a className="nav-stylehead" href="contact.html">Contact</a>
										</li>
									</ul>
								</div>
							</div>
						</nav>
					</div>
				</div>
			</div>
	)
}

export default Navbar;