
//create appComp.js

import { hot  } from 'react-hot-loader/root';
import React from "react";
import App from "./App.js";

const AppComp = (props)=>{
    return (
		<div>
		   <App />  
		</div>
	)
}

export default hot(AppComp)
// export default AppComp