import React, { lazy, Suspense } from "react";
//Pages Imports
import Home from './pages/home/Home.js';
import Dashboard from './pages/Dashboard.js';
import AddToCart from './pages/AddToCart.js';
import Products from './pages/products/Products';
import ProductDetails from './pages/products/ProductDetails';

//Component Imports
import Header from './components/Header.js';
import Footer from './components/Footer.js';
import Notfound from './components/notfound/Notfound'


import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";

export default class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpen: true
        };
    }
 
    render() {
        const { isOpen } = this.state
        return (
			<div>
				<Suspense fallback={<div className="full-page-loader">Loding Text...</div>}>
					<Router>
						<Header/>
						<Switch>
							<Route path="/" component={Home} exact /> 
							<Route path="/add-to-cart" component={AddToCart} exact /> 
							<Route path="/products" component={Products} /> 
							<Route path="/product/ProductDetails" component={ProductDetails} /> 
							<Route path="*" component={Notfound} /> 
						</Switch>
						<Footer/>
					</Router>
				</Suspense>
			</div>
		)
    }
}
