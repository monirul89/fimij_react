const path = require("path");
const webpack = require('webpack');

const config = {
    entry: {
        app:[ "./src/index.js",]
    },
    
    output: {
        path: path.join(__dirname, 'dist'),
        filename: '[name].bundle.js',
        publicPath: '/',
    },
    mode: "production",
    module: {
        rules: [   
			{
                test: /\.css$/,
                use: ["style-loader", "css-loader",]//"sass-loader"
            },		
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: ["babel-loader"]
            }
        ]
    }
};

module.exports = config;