const path = require("path");
var webpack = require('webpack');
const config = {
    entry: [   
        'react-hot-loader/patch',
        'webpack-hot-middleware/client',
        path.resolve(__dirname, "src", "index_dev.js")],
    output: {
        path: path.join(__dirname, 'dist'),
        filename: 'bundle.js',
        publicPath: '/',
    },
    mode: "development",
    devtool: 'inline-source-map',
    module: {
        rules: [
             {
                test: /\.css$/,
                use: ["style-loader", "css-loader",]//"sass-loader"
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: ["babel-loader"]
            }
        ]
    },
    plugins: [
        new webpack.DefinePlugin({
            '__DEV__': JSON.stringify(true)
        }),
        new webpack.HotModuleReplacementPlugin()
    ],
};

module.exports = config;