
import React, { lazy, Suspense } from "react";
import {  Link } from "react-router-dom";

export default class Sales extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpen: true
        };
    }
 
    render() {
        const { isOpen } = this.state
        return (
			<div className="main-content">
                <div class="page-content">
                    <div class="container-fluid">
                        <div class="row"><div class="col-12"><div class="page-title-box d-flex align-items-center justify-content-between"><h4 class="mb-0 font-size-18">Sales</h4><div class="page-title-right"><ol class="breadcrumb m-0"><li class="breadcrumb-item"><a href="javascript: void(0);">Sales</a></li><li class="breadcrumb-item active">Sales</li></ol></div></div></div></div>
                    
                    </div>
                </div>
            </div>
		)
    }
}
