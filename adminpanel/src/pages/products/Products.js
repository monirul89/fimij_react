
import React, { Component, lazy, Suspense } from "react";
import { Link, NavLink } from "react-router-dom";
export default class Products extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpen: true
        };
    }
 
    render() {
        const { isOpen } = this.state
        return (
			<div className="main-content">
                <div className="page-content">
                    <div className="container-fluid">

                        <div className="row">
                            <div className="col-12">
                                <div className="page-title-box d-flex align-items-center justify-content-between">
                                    <h4 className="mb-0 font-size-18">Product List DD</h4>
                                    <div className="page-title-right">
                                        <ol className="breadcrumb m-0">
                                            <li className="breadcrumb-item"><a href="javascript: void(0);">Products</a></li>
                                            <li className="breadcrumb-item active">Product</li>
                                        </ol>
                                    </div>                                    
                                </div>
                            </div>
                            <div className="col-12 create-product">
                                <div className="float-right">
                                    <Link className="btn btn-primary" to="/product/create">Create Product</Link>
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-12">
                                <div className="card">
                                    <div className="card-body">
                                        <h4 className="card-title">Products</h4>
                                        <table id="datatable-buttons" className="table table-striped table-bordered dt-responsive nowrap" style={{borderCollapse: 'collapse', borderSpacing: '0', width: '100%'}}>
                                            <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Thumbnail</th>
                                                    <th>Name</th>
                                                    <th>Price</th>
                                                    <th>Qty</th>
                                                    <th>Status</th>
                                                    <th>Start date</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
        
        
                                            <tbody>
                                                <tr>
                                                    <td>1</td>
                                                    <td>1</td>
                                                    <td>Tiger Nixon</td>
                                                    <td>2504</td>
                                                    <td>1</td>
                                                    <td>0</td>
                                                    <td>2011/04/25</td>
                                                    <td><Link to="#"><i class="bx bx-edit" aria-hidden="true"></i></Link> | <Link to="#"><i class="bx bx-trash-alt" aria-hidden="true"></i></Link></td>
                                                </tr>
                                                <tr>
                                                    <td>2</td>
                                                    <td>2</td>
                                                    <td>Donna Snider</td>
                                                    <td>2504</td>
                                                    <td>5</td>
                                                    <td>1</td>
                                                    <td>2011/01/25</td>
                                                    <td><Link to="#"><i class="bx bx-edit" aria-hidden="true"></i></Link> | <Link to="#"><i class="bx bx-trash-alt" aria-hidden="true"></i></Link></td>
                                                </tr>
                                                <tr>
                                                    <td>2</td>
                                                    <td>2</td>
                                                    <td>Donna Snider</td>
                                                    <td>2504</td>
                                                    <td>5</td>
                                                    <td>1</td>
                                                    <td>2011/01/25</td>
                                                    <td><Link to="#"><i class="bx bx-edit" aria-hidden="true"></i></Link> | <Link to="#"><i class="bx bx-trash-alt" aria-hidden="true"></i></Link></td>
                                                </tr>
                                                <tr>
                                                    <td>2</td>
                                                    <td>2</td>
                                                    <td>Donna Snider</td>
                                                    <td>2504</td>
                                                    <td>5</td>
                                                    <td>1</td>
                                                    <td>2011/01/25</td>
                                                    <td><Link to="#"><i class="bx bx-edit" aria-hidden="true"></i></Link> | <Link to="#"><i class="bx bx-trash-alt" aria-hidden="true"></i></Link></td>
                                                </tr>
                                                <tr>
                                                    <td>2</td>
                                                    <td>2</td>
                                                    <td>Donna Snider</td>
                                                    <td>2504</td>
                                                    <td>5</td>
                                                    <td>1</td>
                                                    <td>2011/01/25</td>
                                                    <td><Link to="#"><i class="bx bx-edit" aria-hidden="true"></i></Link> | <Link to="#"><i class="bx bx-trash-alt" aria-hidden="true"></i></Link></td>
                                                </tr>
                                                <tr>
                                                    <td>2</td>
                                                    <td>2</td>
                                                    <td>Donna Snider</td>
                                                    <td>2504</td>
                                                    <td>5</td>
                                                    <td>1</td>
                                                    <td>2011/01/25</td>
                                                    <td><Link to="#"><i class="bx bx-edit" aria-hidden="true"></i></Link> | <Link to="#"><i class="bx bx-trash-alt" aria-hidden="true"></i></Link></td>
                                                </tr>
                                                <tr>
                                                    <td>2</td>
                                                    <td>2</td>
                                                    <td>Donna Snider</td>
                                                    <td>2504</td>
                                                    <td>5</td>
                                                    <td>1</td>
                                                    <td>2011/01/25</td>
                                                    <td><Link to="#"><i class="bx bx-edit" aria-hidden="true"></i></Link> | <Link to="#"><i class="bx bx-trash-alt" aria-hidden="true"></i></Link></td>
                                                </tr>
                                                <tr>
                                                    <td>2</td>
                                                    <td>2</td>
                                                    <td>Donna Snider</td>
                                                    <td>2504</td>
                                                    <td>5</td>
                                                    <td>1</td>
                                                    <td>2011/01/25</td>
                                                    <td><Link to="#"><i class="bx bx-edit" aria-hidden="true"></i></Link> | <Link to="#"><i class="bx bx-trash-alt" aria-hidden="true"></i></Link></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        

                    </div>
                </div>
            </div>
		)
    }
}
