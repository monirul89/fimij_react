import React, { lazy, Suspense } from "react";
import { Link } from "react-router-dom";

export default class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpen: true
        };
    }
 
    render() {
        const { isOpen } = this.state
        return (
			<div> 
				<div className="home-btn d-none d-sm-block">
            <a href="index.html" className="text-dark"><i className="mdi mdi-home-variant h2"></i></a>
            </div>
            <div className="account-pages my-5 pt-sm-5">
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-md-8 col-lg-6 col-xl-5">
                            <div className="card overflow-hidden">
                                <div className="bg-soft-primary">
                                    <div className="row">
                                        <div className="col-7">
                                            <div className="text-primary p-4">
                                                <h5 className="text-primary">Free Register</h5>
                                                <p>Get your free Skote account now.</p>
                                            </div>
                                        </div>
                                        <div className="col-5 align-self-end">
                                            <img src="assets/images/profile-img.png" alt="" className="img-fluid" />
                                        </div>
                                    </div>
                                </div>
                                <div className="card-body pt-0"> 
                                    <div className="signup-logo">
                                        <a href="/signup">
                                            <div className="avatar-md profile-user-wid mb-4">
                                                <span className="avatar-title rounded-circle bg-light">
                                                    <img src="assets/images/logo.svg" alt="" className="rounded-circle" height="34" />
                                                </span>
                                            </div>
                                        </a>
                                    </div>
                                    <div className="p-2">
                                        <form className="form-horizontal" action="https://themesbrand.com/skote/layouts/vertical/index.html">
                
                                            <div className="form-group">
                                                <label for="useremail">Email</label>
                                                <input type="email" className="form-control" id="useremail" placeholder="Enter email" />        
                                            </div>
                    
                                            <div className="form-group">
                                                <label for="username">Username</label>
                                                <input type="text" className="form-control" id="username" placeholder="Enter username" />
                                            </div>
                    
                                            <div className="form-group">
                                                <label for="userpassword">Password</label>
                                                <input type="password" className="form-control" id="userpassword" placeholder="Enter password" />        
                                            </div>
                        
                                            <div className="mt-4">
                                                <button className="btn btn-primary btn-block waves-effect waves-light" type="submit">Register</button>
                                            </div>

                                            <div className="mt-4 text-center">
                                                <h5 className="font-size-14 mb-3">Sign up using</h5>
                
                                                <ul className="list-inline">
                                                    <li className="list-inline-item">
                                                        <a href="javascript::void()" className="social-list-item bg-primary text-white border-primary">
                                                            <i className="mdi mdi-facebook"></i>
                                                        </a>
                                                    </li>
                                                    <li className="list-inline-item">
                                                        <a href="javascript::void()" className="social-list-item bg-info text-white border-info">
                                                            <i className="mdi mdi-twitter"></i>
                                                        </a>
                                                    </li>
                                                    <li className="list-inline-item">
                                                        <a href="javascript::void()" className="social-list-item bg-danger text-white border-danger">
                                                            <i className="mdi mdi-google"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                    
                                            <div className="mt-4 text-center">
                                                <p className="mb-0">By registering you agree to the Skote <a href="#" className="text-primary">Terms of Use</a></p>
                                            </div>
                                        </form>
                                    </div>
                
                                </div>
                            </div>
                            <div className="mt-5 text-center">
                                
                                <div>
                                    <p>Already have an account ? <a href="/signin" className="font-weight-medium text-primary"> Login</a> </p>
                                    <p>© 2021 Team. Crafted with <i className="mdi mdi-heart text-danger"></i> by Teams</p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
			</div>
		)
    }
}