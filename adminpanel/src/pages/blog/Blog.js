
import React, { lazy, Suspense } from "react";
import {  Link } from "react-router-dom";

export default class Blog extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpen: true
        };
    }
 
    render() {
        const { isOpen } = this.state
        return (
			<div className="main-content">
                    <div className="page-content">
                        <div className="container-fluid">
                            <div className="row"><div className="col-12"><div className="page-title-box d-flex align-items-center justify-content-between"><h4 className="mb-0 font-size-18">Posts</h4><div className="page-title-right"><ol className="breadcrumb m-0"><li className="breadcrumb-item"><a href="javascript: void(0);">Posts</a></li><li className="breadcrumb-item active">Post</li></ol></div></div></div></div>
                            <div className="row">
                                <div className="col-12 categories_items">
                                    <div className="row">
                                        <div className="col-lg-12">
                                            <div className="card">
                                                <div className="card-body">
                                                    <form className="outer-repeater">
                                                        <div data-repeater-list="outer-group" className="outer">
                                                            <div data-repeater-item className="outer">
                                                                <div className="form-group row mb-4">
                                                                    <label for="post_name" className="col-form-label col-lg-2">Title</label>
                                                                    <div className="col-lg-10">
                                                                        <input id="post_name" name="post_name" type="text" className="form-control" placeholder="Enter Name..." />
                                                                    </div>
                                                                </div>
                                                                <div className="form-group row mb-4">
                                                                    <label className="col-form-label col-lg-2">Description</label>
                                                                    <div className="col-lg-10">
                                                                        <div className="summernote">Summernote</div>
                                                                    </div>
                                                                </div>

                                                                <div className="form-group row mb-4">
                                                                    <label className="col-form-label col-lg-2">Date</label>
                                                                    <div className="col-lg-10">
                                                                        <div className="input-daterange input-group" data-provide="datepicker">
                                                                            <input type="date" placeholder="Date" name="date" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                    <div className="row justify-content-end">
                                                        <div className="col-lg-10">
                                                            <button type="submit" className="btn btn-primary" name="save_post">Save</button>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
		)
    }
}