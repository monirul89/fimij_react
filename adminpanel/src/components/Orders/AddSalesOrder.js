import React, { useState, useEffect } from "react";
import { Link, Redirect } from "react-router-dom";
import Swal from 'sweetalert2';
const axios = require('axios');

const AddSalesOrder = () => {
    const [orderNumber, setOrderNumber] = useState(null);
    const [status, setStatus] = useState(true);

    const createBrand = (e) => {
        e.preventDefault();

        axios.post("http://127.0.0.1:5000/admin/order", {
            orderNumber: setOrderNumber,
            status: status ? true : false
        }) 
        .then((response) => {
            setOrderNumber("");
            setStatus(true);
            Swal.fire({
                icon: 'success',
                title: "Sales order successfully created",
                showConfirmButton: false,
                timer: 3000
            })
        }).catch((err) => {
            Swal.fire({
                icon: 'error',
                title: err.message,
                showConfirmButton: false,
                timer: 3000
            })
        });
    }

    return (
        <div className="main-content">
            <div className="page-content">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-12">
                            <div className="page-title-box d-flex align-items-center justify-content-between">
                                <h4 className="mb-0 font-size-18">Add Sales Order</h4>
                                <div className="page-title-right">
                                    <ol className="breadcrumb m-0">
                                        <li className="breadcrumb-item"><a href="javascript: void(0);">Add Sales Order</a></li>
                                        <li className="breadcrumb-item active">Add Sales Order</li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-9 categories_body">
                            <div className="category_option card_option">
                                <div className="card">

                                    <div className="card-body">
                                        <div className="form-group row mb-4">
                                            <label for="horizontal-firstname-input" className="col-sm-3 col-form-label">Order Number<span class="m-l-5 text-red">*</span></label>
                                            <div className="col-md-7 col-sm-12">
                                                <input type="text" className="form-control" id="horizontal-firstname-input" value={orderNumber} onChange={(e) => setOrderNumber(e.target.value)} />
                                            </div>
                                        </div>
                                        <div className="form-group row mb-4">
                                            <label for="horizontal-password-input" className="col-sm-3 col-form-label">Status</label>
                                            <div className="text-box col-md-7 col-sm-12">
                                                <input type="checkbox" name="category_status" id="category_status" onChange={(e) => setStatus(e.target.value ? true : false)} />
                                                <label for="category_status">Enable the order?</label>
                                            </div>
                                        </div>

                                        <div className="form-group row justify-content-end">
                                            <label for="horizontal-email-input" className="col-sm-3 col-form-label"></label>
                                            <div className="col-md-9 col-sm-12">
                                                <div>
                                                    <button onClick={createBrand} className="btn btn-primary w-md">Save</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}


export default AddSalesOrder;