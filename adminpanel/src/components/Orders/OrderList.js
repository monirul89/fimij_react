import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
const axios = require('axios');

const OrderList = () => {
    useEffect(() => {
        document.title = "Order List - Admin Panel";
    }, []);
    
    const [orders, setOrders] = useState([]); 

    useEffect(() => {
        axios.get("http://127.0.0.1:5000/admin/order")
        .then((response) => {
            console.log(response.data)
            setOrders(response.data);
        }) 
    }, []); 

    return (
        <div className="main-content">
            <div className="page-content">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-12">
                            <div className="page-title-box d-flex align-items-center justify-content-between">
                                <h4 className="mb-0 font-size-18">Order List</h4>
                                <div className="page-title-right">
                                    <ol className="breadcrumb m-0">
                                        <li className="breadcrumb-item"><a href="javascript: void(0);">Order List</a></li>
                                        <li className="breadcrumb-item active">Order</li>
                                    </ol>
                                </div>                                    
                            </div>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-12">
                            <div className="card">
                                <div className="card-body"> 
                                    <table id="datatable-buttons" className="table table-striped table-bordered dt-responsive nowrap" style={{borderCollapse: 'collapse', borderSpacing: '0', width: '100%'}}>
                                        <thead>
                                            <tr>
                                                <th>SL</th>
                                                <th>Order Number</th>
                                                <th>Masking Number</th>                                                
                                                <th>Grand Total</th>
                                                <th>Order Status</th>
                                                <th>Customer</th> 
                                                <th>Order Date</th> 
                                            </tr>
                                        </thead> 
                                        <tbody> 
                                           {
                                               orders.map((data,index) => (
                                                    <tr key={ data._id }>
                                                        <td>{ index+1 }</td>
                                                        <td>{ data.orderNumber }</td>
                                                        <td>{ data.maskingNumber }</td>
                                                        <td>{ data.grandTotal }</td>
                                                        <td>{ data.orderStatus }</td>
                                                        <td>{ data.customer ? data.customer.name : data.customer }</td> 
                                                        <td>{ data.createdAt }</td> 
                                                    </tr> 
                                               ))
                                           }
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    )
}


export default OrderList;