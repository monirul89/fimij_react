import React from "react";
import Headertop from './Headertop';
import Verticalmenu from '../components/Verticalmenu';
import Rightsidebar from '../components/Rightsidebar';
const Header = () => {
	return (
		<div>

			<Headertop />

            <Verticalmenu />
            
			<Rightsidebar/>
			
		</div>
	)
}

export default Header;