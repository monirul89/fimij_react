import React, { useState, useEffect } from "react";
import Swal from 'sweetalert2';
const axios = require('axios');

const AddProduct = () => {
    const [categorys, setCategoryList] = useState([]); 
    const [brands, setBrandList] = useState([]); 

    useEffect(() => {
        axios.get("http://127.0.0.1:5000/admin/category")
        .then((response) => {
            setCategoryList(response.data.data);
        }) 

        axios.get("http://127.0.0.1:5000/admin/brand")
        .then((response) => {
            setBrandList(response.data);
        }) 
    }, []); 

    const [productName, setProductName] = useState("");
    const [category, setCategory] = useState("");
    const [brand, setBrand] = useState("");
    const [origin, setOrigin] = useState("");
    const [color, setColor] = useState("");
    const [description, setDescription] = useState("");
    const [totalQty, setTotalQty] = useState(0);
    const [purchasedPrice, setPurchasedPrice] = useState(0);
    const [salePrice, setSalePrice] = useState(0);
    const [isMart, setIsMart] = useState(true);
    const [youtubeVideoLink, setYoutubeVideoLink] = useState("");
    const [productImage, setProductImage] = useState("");
    const [status, setStatus] = useState(true);

    const createProduct = (e) => {
        e.preventDefault();

        axios.post("http://127.0.0.1:5000/admin/product", {
            productName: productName,
            category: category,
            brand: brand,
            origin: origin,
            color: color,
            description: description,
            totalQty: totalQty,
            purchasedPrice: purchasedPrice,
            salePrice: salePrice,
            isMart: isMart,
            youtubeVideoLink: youtubeVideoLink,
            productImage: productImage,
            status: status
        })
        .then((response) => {
            Swal.fire({
                icon: 'success',
                title: 'Category create successfully.',
                showConfirmButton: false,
                timer: 3000
            }) 
        }).catch((err) => {
            Swal.fire({
                icon: 'error',
                title: err.message,
                showConfirmButton: false,
                timer: 3000
            }) 
        });
    }

    return (
        <div className="main-content">
            <div className="page-content">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-12">
                            <div className="page-title-box d-flex align-items-center justify-content-between">
                                <h4 className="mb-0 font-size-18">Add Product</h4>
                                <div className="page-title-right">
                                    <ol className="breadcrumb m-0">
                                        <li className="breadcrumb-item"><a href="javascript: void(0);">Add Product</a></li>
                                        <li className="breadcrumb-item active">Add Product</li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-9 categories_body">
                            <div className="category_option card_option">
                                <div className="card">

                                    <div className="card-body">
                                        <div className="form-group row mb-4">
                                            <label for="horizontal-firstname-input" className="col-sm-3 col-form-label">Product Name<span class="m-l-5 text-red">*</span></label>
                                            <div className="col-md-7 col-sm-12">
                                                <input type="text" className="form-control" id="horizontal-firstname-input" value={productName} onChange={(e) => setProductName(e.target.value)} required />
                                            </div>
                                        </div>
                                        <div className="form-group row mb-4">
                                            <label for="category" className="col-sm-3 col-form-label">Category<span class="m-l-5 text-red">*</span></label>
                                            <div className="col-md-7 col-sm-12">
                                                <select name="category" className="form-control" onChange={(e) => setCategory(e.target.value)}>
                                                    {
                                                        categorys.map((data) => (
                                                            <option key={data._id} value={ data.catName }> { data.catName } </option>
                                                        ))
                                                    }
                                                </select> 
                                            </div>
                                        </div>
                                        <div className="form-group row mb-4">
                                            <label for="origin" className="col-sm-3 col-form-label">Origin<span class="m-l-5 text-red">*</span></label>
                                            <div className="col-md-7 col-sm-12">
                                                <select name="origin" className="form-control" onChange={(e) => setOrigin(e.target.value)}>
                                                    <option value="Own">Own</option>
                                                    <option value="Third Party">Third Party</option>
                                                </select> 
                                            </div>
                                        </div>
                                        <div className="form-group row mb-4">
                                            <label for="horizontal-firstname-input" className="col-sm-3 col-form-label">Brand<span class="m-l-5 text-red">*</span></label>
                                            <div className="col-md-7 col-sm-12">
                                                <select name="brand" className="form-control" onChange={(e) => setBrand(e.target.value)}>
                                                    {
                                                        brands.map((data) => (
                                                            <option key={data._id} value={ data.brandName }> { data.brandName } </option>
                                                        ))
                                                    }
                                                </select> 
                                            </div>
                                        </div>
                                        <div className="form-group row mb-4">
                                            <label for="horizontal-firstname-input" className="col-sm-3 col-form-label">Color<span class="m-l-5 text-red">*</span></label>
                                            <div className="col-md-7 col-sm-12">
                                                <input type="text" className="form-control" id="horizontal-firstname-input" value={color} onChange={(e) => setColor(e.target.value)} />
                                            </div>
                                        </div>
                                        <div className="form-group row mb-4">
                                            <label for="horizontal-firstname-input" className="col-sm-3 col-form-label">Description</label>
                                            <div className="col-md-7 col-sm-12">
                                                <input type="text" className="form-control" id="horizontal-firstname-input" value={description} onChange={(e) => setDescription(e.target.value)} />
                                            </div>
                                        </div>
                                        <div className="form-group row mb-4">
                                            <label for="horizontal-firstname-input" className="col-sm-3 col-form-label">Total Qty<span class="m-l-5 text-red">*</span></label>
                                            <div className="col-md-7 col-sm-12">
                                                <input type="number" className="form-control" id="horizontal-firstname-input" value={totalQty} onChange={(e) => setTotalQty(e.target.value)} />
                                            </div>
                                        </div>
                                        <div className="form-group row mb-4">
                                            <label for="horizontal-firstname-input" className="col-sm-3 col-form-label">Purchased Price<span class="m-l-5 text-red">*</span></label>
                                            <div className="col-md-7 col-sm-12">
                                                <input type="text" className="form-control" id="horizontal-firstname-input" value={purchasedPrice} onChange={(e) => setPurchasedPrice(e.target.value)} />
                                            </div>
                                        </div>
                                        <div className="form-group row mb-4">
                                            <label for="horizontal-firstname-input" className="col-sm-3 col-form-label">Sales Price<span class="m-l-5 text-red">*</span></label>
                                            <div className="col-md-7 col-sm-12">
                                                <input type="text" className="form-control" id="horizontal-firstname-input" value={salePrice} onChange={(e) => setSalePrice(e.target.value)} />
                                            </div>
                                        </div>
                                        <div className="form-group row mb-4">
                                            <label for="horizontal-firstname-input" className="col-sm-3 col-form-label">Youtube Video Link</label>
                                            <div className="col-md-7 col-sm-12">
                                                <input type="text" className="form-control" id="horizontal-firstname-input" value={youtubeVideoLink} onChange={(e) => setYoutubeVideoLink(e.target.value)} />
                                            </div>
                                        </div>
                                        <div className="form-group row mb-4">
                                            <label for="horizontal-firstname-input" className="col-sm-3 col-form-label">Image</label>
                                            <div className="col-md-7 col-sm-12">
                                                <input type="file" className="form-control" id="productImage" value={productImage} onChange={(e) => setProductImage(e.target.value)} />
                                            </div>
                                        </div>
                                        <div className="form-group row mb-4">
                                            <label for="isMart" className="col-sm-3 col-form-label">Is Mart</label>
                                            <div className="text-box col-md-7 col-sm-12">
                                                <input type="checkbox" name="isMart" id="isMart" onChange={(e) => setIsMart(e.target.value ? true : false)} />
                                                <label for="isMart">Enable the Mart?</label>
                                            </div>
                                        </div>
                                        <div className="form-group row mb-4">
                                            <label for="productStatus" className="col-sm-3 col-form-label">Status</label>
                                            <div className="text-box col-md-7 col-sm-12">
                                                <input type="checkbox" name="productStatus" id="productStatus" onChange={(e) => setStatus(e.target.value ? true : false)} />
                                                <label for="productStatus">Enable the status?</label>
                                            </div>
                                        </div>

                                        <div className="form-group row justify-content-end">
                                            <label for="horizontal-email-input" className="col-sm-3 col-form-label"></label>
                                            <div className="col-md-9 col-sm-12">
                                                <div>
                                                    <button onClick={createProduct} className="btn btn-primary w-md">Save</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}


export default AddProduct;