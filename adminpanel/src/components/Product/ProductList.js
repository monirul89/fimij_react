import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
const axios = require('axios');

const ProductList = () => {
    const [products, setProducts] = useState([]); 

    useEffect(() => {
        axios.get("http://127.0.0.1:5000/admin/product")
        .then((response) => {
            console.log(response.data)
            setProducts(response.data);
        }) 
    }, []); 

    return (
        <div className="main-content">
            <div className="page-content">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-12">
                            <div className="page-title-box d-flex align-items-center justify-content-between">
                                <h4 className="mb-0 font-size-18">Product List</h4>
                                <div className="page-title-right">
                                    <ol className="breadcrumb m-0">
                                        <li className="breadcrumb-item"><a href="javascript: void(0);">Product List</a></li>
                                        <li className="breadcrumb-item active">Product</li>
                                    </ol>
                                </div>                                    
                            </div>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-12">
                            <div className="card">
                                <div className="card-body"> 
                                    <table id="datatable-buttons" className="table table-striped table-bordered dt-responsive nowrap" style={{borderCollapse: 'collapse', borderSpacing: '0', width: '100%'}}>
                                        <thead>
                                            <tr>
                                                <th>SL</th>
                                                <th>Product Name</th>
                                                <th>Category</th>
                                                <th>Brand</th>
                                                <th>Remain Qty</th>
                                                <th>Sale Price</th>
                                                <th>Status</th>
                                                <th>Created Date</th> 
                                                <th>Action</th>
                                            </tr>
                                        </thead> 
                                        <tbody> 
                                           {
                                               products.map((data,index) => (
                                                    <tr key={ data._id }>
                                                        <td>{ index+1 }</td>
                                                        <td>{ data.productName }</td>
                                                        <td>{ data.category }</td>
                                                        <td>{ data.brand }</td>
                                                        <td>{ data.inventory.remainQty }</td>
                                                        <td>{ data.inventory.salePrice }</td>                                                        
                                                        <td>{ data.status ? 'Active' : 'Inactive'}</td>
                                                        <td>{ data.createdAt }</td>
                                                        <td><Link to="#"><i class="bx bx-edit" aria-hidden="true"></i></Link> | <Link to="#"><i class="bx bx-trash-alt" aria-hidden="true"></i></Link></td>
                                                    </tr> 
                                               ))
                                           }
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    )
}


export default ProductList;