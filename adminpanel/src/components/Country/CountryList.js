import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
const axios = require('axios');

const CountryList = () => {
    const [countries, setCountries] = useState([]); 

    useEffect(() => {
        axios.get("http://127.0.0.1:5000/admin/country")
        .then((response) => { 
            setCountries(response.data);
        }).catch((err) => {
            setMessage(err);
        });
      }, []); 

    return (
        <div className="main-content">
            <div className="page-content">
                <div className="container-fluid"> 
                    <div className="row">
                        <div className="col-12">
                            <div className="page-title-box d-flex align-items-center justify-content-between">
                                <h4 className="mb-0 font-size-18">Country List</h4>
                                <div className="page-title-right">
                                    <ol className="breadcrumb m-0">
                                        <li className="breadcrumb-item"><a href="javascript: void(0);">Country List</a></li>
                                        <li className="breadcrumb-item active">Country List</li>
                                    </ol>
                                </div>                                    
                            </div>
                        </div> 
                    </div>

                    <div className="row">
                        <div className="col-12">
                            <div className="card">
                                <div className="card-body">
                                    <h4 className="card-title">Country List</h4>
                                    <table id="datatable-buttons" className="table table-striped table-bordered dt-responsive nowrap" style={{borderCollapse: 'collapse', borderSpacing: '0', width: '100%'}}>
                                        <thead>
                                            <tr>
                                                <th>SL</th>
                                                <th>Country Name</th>
                                                <th>Status</th>
                                                <th>Created Date</th> 
                                                <th>Action</th>
                                            </tr>
                                        </thead> 
                                        <tbody> 
                                           {
                                                countries.map((data,index) => (
                                                    <tr key={ data.id }>
                                                        <td>{ index+1 }</td>
                                                        <td>{ data.countryName }</td>
                                                        <td>{ data.status ? 'Active' : 'Inactive'}</td>
                                                        <td>{ data.createdAt }</td>
                                                        <td><Link to="#">Edit</Link> | <Link to="#">Delete</Link></td>
                                                    </tr> 
                                                ))
                                           }
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    )
}


export default CountryList;