import React from "react";
import { Link, NavLink } from "react-router-dom";

const Verticalmenu = () => {
	return (
		<div className="vertical-menu">

                <div data-simplebar className="h-100">

                    <div id="sidebar-menu">
                        {/* <!-- Left Menu Start --> */}
                        <ul className="metismenu list-unstyled" id="side-menu">

                            <li>
                                <Link to="/admin" className="waves-effect">
                                    <i className="bx bx-home-circle"></i>
                                    <span key="t-dashboards">Dashboard</span>
                                </Link>
                            </li> 
                            <li>
                                <Link className="has-arrow waves-effect" >
                                    <i className="bx bx-store"></i>
                                    <span key="t-ecommerce">Products</span>
                                </Link>
                                <ul className="sub-menu" aria-expanded="false">
                                    <li><Link to="/products/add" key="t-add-product">Add Product</Link></li>
                                    <li><a href="/products/list" >Product List</a></li>                                     
                                </ul>
                            </li>
                            <li>
                                <Link to="#" className="has-arrow waves-effect">
                                    <i className="bx bx-bitcoin"></i>
                                    <span key="t-ecommerce">Sales</span>
                                </Link>
                                <ul className="sub-menu" aria-expanded="false">
                                    <li><Link to="/orders/add" key="t-add-product">Add Sales Order</Link></li>
                                    <li><Link to="/orders/list">Sales Order List</Link></li>                                     
                                </ul>
                            </li> 
                            <li>
                                <Link to="#" className="has-arrow waves-effect">
                                    <i className="bx bx-bitcoin"></i>
                                    <span key="t-ecommerce">Customers</span>
                                </Link>
                                <ul className="sub-menu" aria-expanded="false">
                                    <li><Link to="/customer/add" key="t-add-product">Add Customer</Link></li>
                                    <li><Link to="/customer/list">Customer List</Link></li>                                     
                                </ul>
                            </li> 
                            <li>
                                <Link to="#" className="has-arrow waves-effect">
                                    <i className="bx bx-envelope"></i>
                                    <span key="t-email">Blogs</span>
                                </Link>
                                <ul className="sub-menu" aria-expanded="false">
                                    <li> <Link to="/blog/add" target="_blank" key="t-inbox">Create Post</Link></li>
                                    <li> <Link to="/blog/list" key="t-read-email">Post List</Link></li>
                                </ul>
                            </li>                         

                            <li>
                                <Link to="#" className="has-arrow waves-effect">
                                <i className="bx bxs-user-detail"></i>
                                    <span key="t-email">Reports</span>
                                </Link> 
                                <ul className="sub-menu" aria-expanded="false">
                                    <li><Link to="/report/customer" key="t-inbox">Customer Report</Link></li>
                                    <li><Link to="/report/sales-order" key="t-inbox">Sales Order Repot</Link></li>
                                    <li><Link to="/report/product" key="t-inbox">Product Report</Link></li> 
                                </ul>
                            </li> 

                            <li>
                                <Link to="#" className="has-arrow waves-effect">
                                    <i className="bx bx-layout"></i>
                                    <span key="t-layouts">Category</span>
                                </Link>
                                <ul className="sub-menu" aria-expanded="false">
                                    <li><Link to="/category/add" key="t-horizontal">Add Category</Link></li>
                                    <li><Link to="/category/list" key="t-vertical">Category List</Link></li>
                                </ul>
                            </li>
                            <li>
                                <Link to="#" className="has-arrow waves-effect">
                                    <i className="bx bx-layout"></i>
                                    <span key="t-layouts">Brand</span>
                                </Link>
                                <ul className="sub-menu" aria-expanded="false">
                                    <li><Link to="/brand/add" key="t-horizontal">Add Brand</Link></li>
                                    <li><Link to="/brand/list" key="t-vertical">Brand List</Link></li>
                                </ul>
                            </li>
                            <li>
                                <Link to="#" className="has-arrow waves-effect">
                                    <i className="bx bx-layout"></i>
                                    <span key="t-layouts">Country</span>
                                </Link>
                                <ul className="sub-menu" aria-expanded="false">
                                    <li><Link to="/country/add" key="t-horizontal">Add Country</Link></li>
                                    <li><Link to="/country/list" key="t-vertical">Country List</Link></li>
                                </ul>
                            </li>
                            <li>
                                <Link to="#" className="has-arrow waves-effect">
                                    <i className="bx bx-layout"></i>
                                    <span key="t-layouts">Coupon</span>
                                </Link>
                                <ul className="sub-menu" aria-expanded="false">
                                    <li><Link to="/coupon/add" key="t-horizontal">Add Coupon</Link></li>
                                    <li><Link to="/coupon/list" key="t-vertical">Coupon List</Link></li>
                                </ul>
                            </li>

                        </ul>
                    </div>
                    {/* <!-- Sidebar --> */}
                </div>
            </div>
		
	)
}

export default Verticalmenu;