import React, { useState, useEffect } from "react";
import Swal from 'sweetalert2';
const axios = require('axios');

const AddCategory = (props) => {
    const id = props.match.params.id;
    let editAble = false;
    if (id) editAble = true;
    const [category, setCategory] = useState({
        catName: "",
        status: true
    });
    const [status, setStatus] = useState(true);

    const createCategory = (e) => {
        e.preventDefault();

        if (editAble){
            axios.put(`http://127.0.0.1:5000/admin/category/${id}`, category)
                .then((response) => {
                    Swal.fire({
                        icon: 'success',
                        title: 'Category Update successfully.',
                        showConfirmButton: false,
                        timer: 3000
                    }).then(function() {
                        window.location = "/category/list";
                    });
                }).catch((err) => {
                Swal.fire({
                    icon: 'error',
                    title: err.message,
                    showConfirmButton: false,
                    timer: 3000
                }).then(function() {
                    window.location = "/category/list";
                });
            });
        }else {
            axios.post("http://127.0.0.1:5000/admin/category", category)
                .then((response) => {
                    Swal.fire({
                        icon: 'success',
                        title: 'Category create successfully.',
                        showConfirmButton: false,
                        timer: 3000
                    })
                }).catch((err) => {
                Swal.fire({
                    icon: 'error',
                    title: err.message,
                    showConfirmButton: false,
                    timer: 3000
                })
            });
        }
        
    };

    const onChangeHandeler = (e) => {
        setCategory({...category, [e.target.name]: e.target.value});
    }

    useEffect(() => {
        if (editAble){
            axios.get(`http://127.0.0.1:5000/admin/category/${id}`)
                .then((response) => {
                    const {catName, status} = response.data.data;
                    setCategory({catName, status});
                }).catch((err) => {
                Swal.fire({
                    icon: 'error',
                    title: err.message,
                    showConfirmButton: false,
                    timer: 3000
                })
            });
        }
    },[])

    return (
        <div className="main-content">
            <div className="page-content">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-12">
                            <div className="page-title-box d-flex align-items-center justify-content-between">
                                <h4 className="mb-0 font-size-18">{editAble ? 'Edit Category' : 'Add Category'}</h4>
                                <div className="page-title-right">
                                    <ol className="breadcrumb m-0">
                                        <li className="breadcrumb-item"><a href="javascript: void(0);">{editAble ? 'Edit Category' : 'Add Category'}</a></li>
                                        <li className="breadcrumb-item active">{editAble ? 'Edit Category' : 'Add Category'}</li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-9 categories_body">
                            <div className="category_option card_option">
                                <div className="card">

                                    <div className="card-body">
                                        <div className="form-group row mb-4">
                                            <label for="horizontal-firstname-input" className="col-sm-3 col-form-label">Category Name<span class="m-l-5 text-red">*</span></label>
                                            <div className="col-md-7 col-sm-12">
                                                <input type="text" className="form-control" id="horizontal-firstname-input" name="catName" value={category.catName} onChange={(e) => onChangeHandeler(e)} />
                                            </div>
                                        </div>
                                        <div className="form-group row mb-4">
                                            <label for="horizontal-password-input" className="col-sm-3 col-form-label">Status</label>
                                            <div className="text-box col-md-7 col-sm-12">
                                                <input type="checkbox" name="category_status" id="category_status" checked={category.status} onChange={() => setCategory({...category, status: !category.status})} />
                                                <label for="category_status">Enable the category?</label>
                                            </div>
                                        </div>

                                        <div className="form-group row justify-content-end">
                                            <label for="horizontal-email-input" className="col-sm-3 col-form-label"></label>
                                            <div className="col-md-9 col-sm-12">
                                                <div>
                                                    <button onClick={createCategory} className="btn btn-primary w-md">{editAble ? 'Update' : 'Save'}</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}


export default AddCategory;