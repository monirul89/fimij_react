import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
const axios = require('axios');

const CategoryList = () => {
    const [category, setCategory] = useState([]); 

    useEffect(() => {
        axios.get("http://127.0.0.1:5000/admin/category")
        .then((response) => {
            console.log(response.data.data)
            setCategory(response.data.data);
        }).catch((err) => {
            setMessage(err);
        });
    }, []); 

    const deleteCategory = (e) => {
        console.log(e)
        axios.delete("http://127.0.0.1:5000/admin/category",{
            catId: e
        })
    }    

    return (
        <div className="main-content">
            <div className="page-content">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-12">
                            <div className="page-title-box d-flex align-items-center justify-content-between">
                                <h4 className="mb-0 font-size-18">Category List</h4>
                                <div className="page-title-right">
                                    <ol className="breadcrumb m-0">
                                        <li className="breadcrumb-item"><a href="javascript: void(0);">Category List</a></li>
                                        <li className="breadcrumb-item active">Category</li>
                                    </ol>
                                </div>                                    
                            </div>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-12">
                            <div className="card">
                                <div className="card-body">
                                    <h4 className="card-title">Category List</h4>
                                    <table id="datatable-buttons" className="table table-striped table-bordered dt-responsive nowrap" style={{borderCollapse: 'collapse', borderSpacing: '0', width: '100%'}}>
                                        <thead>
                                            <tr>
                                                <th>SL</th>
                                                <th>Category Name</th>
                                                <th>Status</th>
                                                <th>Created Date</th> 
                                                <th>Action</th>
                                            </tr>
                                        </thead> 
                                        <tbody>
                                           {
                                               category.map((data,index) => (
                                                    <tr key={ data._id }>
                                                        <td>{ index+1 }</td>
                                                        <td>{ data.catName }</td>
                                                        <td>{ data.status ? 'Active' : 'Inactive'}</td>
                                                        <td>{ data.createdAt }</td>
                                                        <td><Link to={"/category/edit/"+data._id} >Edit</Link> | <Link to="#">Delete</Link></td>
                                                    </tr> 
                                               ))
                                           }
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    )
}


export default CategoryList;