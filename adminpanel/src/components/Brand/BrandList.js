import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
const axios = require('axios');

const BrandList = () => {
    const [brands, setBrands] = useState([]); 

    useEffect(() => {
        axios.get("http://127.0.0.1:5000/admin/brand")
        .then((response) => { 
            setBrands(response.data);
        }).catch((err) => {
            setMessage(err);
        });
      }, []); 

    return (
        <div className="main-content">
            <div className="page-content">
                <div className="container-fluid"> 
                    <div className="row">
                        <div className="col-12">
                            <div className="page-title-box d-flex align-items-center justify-content-between">
                                <h4 className="mb-0 font-size-18">Brand List</h4>
                                <div className="page-title-right">
                                    <ol className="breadcrumb m-0">
                                        <li className="breadcrumb-item"><a href="javascript: void(0);">Brand List</a></li>
                                        <li className="breadcrumb-item active">Brand</li>
                                    </ol>
                                </div>                                    
                            </div>
                        </div>
                        <div className="col-12 create-product">
                            <div className="float-right">
                                <Link className="btn btn-primary" to="/brand/add">Add Brand</Link>
                            </div>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-12">
                            <div className="card">
                                <div className="card-body">
                                    <h4 className="card-title">Brand List</h4>
                                    <table id="datatable-buttons" className="table table-striped table-bordered dt-responsive nowrap" style={{borderCollapse: 'collapse', borderSpacing: '0', width: '100%'}}>
                                        <thead>
                                            <tr>
                                                <th>SL</th>
                                                <th>Brand Name</th>
                                                <th>Status</th>
                                                <th>Created Date</th> 
                                                <th>Action</th>
                                            </tr>
                                        </thead> 
                                        <tbody> 
                                           {
                                                brands.map((data,index) => (
                                                    <tr key={ data.id }>
                                                        <td>{ index+1 }</td>
                                                        <td>{ data.brandName }</td>
                                                        <td>{ data.status ? 'Active' : 'Inactive'}</td>
                                                        <td>{ data.createdAt }</td>
                                                        <td><Link to="#">Edit</Link> | <Link to="#">Delete</Link></td>
                                                    </tr> 
                                                ))
                                           }
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    )
}


export default BrandList;