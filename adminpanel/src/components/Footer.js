import React from "react";


const Footer = () => {
	return (
		<footer class="footer">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-6">
						<script>document.write(new Date().getFullYear())</script> © Team
					</div>
					<div class="col-sm-6">
						<div class="text-sm-right d-none d-sm-block">
							Design and Develop by Team
						</div>
					</div>
				</div>
			</div>
		</footer>
		
	)
}

export default Footer;