import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
const axios = require('axios');

const CustomerList = () => {
    const [customers, setCustomers] = useState([]); 

    useEffect(() => {
        axios.get("http://127.0.0.1:5000/admin/customer")
        .then((response) => {
            setCustomers(response.data);
        }) 
    }, []); 

    return (
        <div className="main-content">
            <div className="page-content">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-12">
                            <div className="page-title-box d-flex align-items-center justify-content-between">
                                <h4 className="mb-0 font-size-18">Customer List</h4>
                                <div className="page-title-right">
                                    <ol className="breadcrumb m-0">
                                        <li className="breadcrumb-item"><a href="javascript: void(0);">Customer List</a></li>
                                        <li className="breadcrumb-item active">Customer</li>
                                    </ol>
                                </div>                                    
                            </div>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-12">
                            <div className="card">
                                <div className="card-body"> 
                                    <table id="datatable-buttons" className="table table-striped table-bordered dt-responsive nowrap" style={{borderCollapse: 'collapse', borderSpacing: '0', width: '100%'}}>
                                        <thead>
                                            <tr>
                                                <th>SL</th>
                                                <th>Name</th>
                                                <th>Mobile</th>
                                                <th>Email</th>
                                                <th>City</th>
                                                <th>Type</th>
                                                <th>Status</th>
                                                <th>Created Date</th> 
                                                <th>Action</th>
                                            </tr>
                                        </thead> 
                                        <tbody> 
                                           {
                                               customers.map((data,index) => (
                                                    <tr key={ data._id }>
                                                        <td>{ index+1 }</td>
                                                        <td>{ data.name }</td>
                                                        <td>{ data.mobile }</td>
                                                        <td>{ data.email }</td>
                                                        <td>{ data.city }</td>
                                                        <td>{ data.customerType }</td>                                                        
                                                        <td>{ data.status ?  <span className="bg-success active"></span> : <span className="bg-danger inactive"></span> }</td>
                                                        <td>{ data.createdAt }</td>
                                                        <td><Link to="#"><i class="bx bx-edit" aria-hidden="true"></i></Link> | <Link to="#"><i class="bx bx-trash-alt" aria-hidden="true"></i></Link></td>
                                                    </tr> 
                                               ))
                                           }
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    )
}


export default CustomerList;