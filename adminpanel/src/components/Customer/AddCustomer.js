import React, { useState, useEffect } from "react";
import { Link, Redirect } from "react-router-dom";
import Swal from 'sweetalert2';
const axios = require('axios');

const AddCustomer = () => {
    const [countries, setCountryList] = useState([]); 

    useEffect(() => {
        axios.get("http://127.0.0.1:5000/admin/country")
        .then((response) => {
            setCountryList(response.data);
        }) 
    }, []); 

    const [name, setName] = useState("");
    const [mobile, setMobile] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [city, setCity] = useState("");
    const [country, setCountry] = useState("");
    const [zipcode, setZipcode] = useState("");
    const [area, setArea] = useState("");
    const [customerType, setCustomerType] = useState("");
    const [status, setStatus] = useState(true);

    const createBrand = (e) => {
        e.preventDefault();

        axios.post("http://127.0.0.1:5000/admin/customer", {
            name: name,
            mobile: mobile,
            email: email,
            password: password,
            city: city,
            country: country,
            zipcode: zipcode,
            area: area,
            customerType: customerType,
            status: status ? true : false
        }) 
        .then((response) => {
            setName("");
            setMobile("");
            setEmail("");
            setPassword("");
            setCity("");
            setCountry("");
            setZipcode("");
            setArea("");
            setCustomerType("");
            setStatus(true);
            
            Swal.fire({
                icon: 'success',
                title: "Brand successfully created",
                showConfirmButton: false,
                timer: 3000
            })
        }).catch((err) => {
            Swal.fire({
                icon: 'error',
                title: err.message,
                showConfirmButton: false,
                timer: 3000
            })
        });
    }

    return (
        <div className="main-content">
            <div className="page-content">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-12">
                            <div className="page-title-box d-flex align-items-center justify-content-between">
                                <h4 className="mb-0 font-size-18">Add Customer</h4>
                                <div className="page-title-right">
                                    <ol className="breadcrumb m-0">
                                        <li className="breadcrumb-item"><a href="javascript: void(0);">Add Customer</a></li>
                                        <li className="breadcrumb-item active">Add Customer</li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-9 categories_body">
                            <div className="category_option card_option">
                                <div className="card">

                                    <div className="card-body">
                                        <div className="form-group row mb-4">
                                            <label for="horizontal-firstname-input" className="col-sm-3 col-form-label">Name<span class="m-l-5 text-red">*</span></label>
                                            <div className="col-md-7 col-sm-12">
                                                <input type="text" className="form-control" id="horizontal-firstname-input" value={name} onChange={(e) => setName(e.target.value)} />
                                            </div>
                                        </div>
                                        <div className="form-group row mb-4">
                                            <label for="horizontal-firstname-input" className="col-sm-3 col-form-label">Mobile<span class="m-l-5 text-red">*</span></label>
                                            <div className="col-md-7 col-sm-12">
                                                <input type="text" className="form-control" id="horizontal-firstname-input" value={mobile} onChange={(e) => setMobile(e.target.value)} />
                                            </div>
                                        </div>
                                        <div className="form-group row mb-4">
                                            <label for="horizontal-firstname-input" className="col-sm-3 col-form-label">Email<span class="m-l-5 text-red">*</span></label>
                                            <div className="col-md-7 col-sm-12">
                                                <input type="text" className="form-control" id="horizontal-firstname-input" value={email} onChange={(e) => setEmail(e.target.value)} />
                                            </div>
                                        </div>
                                        <div className="form-group row mb-4">
                                            <label for="horizontal-firstname-input" className="col-sm-3 col-form-label">Password<span class="m-l-5 text-red">*</span></label>
                                            <div className="col-md-7 col-sm-12">
                                                <input type="password" className="form-control" id="horizontal-firstname-input" value={password} onChange={(e) => setPassword(e.target.value)} />
                                            </div>
                                        </div>
                                        <div className="form-group row mb-4">
                                            <label for="horizontal-firstname-input" className="col-sm-3 col-form-label">City<span class="m-l-5 text-red">*</span></label>
                                            <div className="col-md-7 col-sm-12">
                                                <input type="text" className="form-control" id="horizontal-firstname-input" value={city} onChange={(e) => setCity(e.target.value)} />
                                            </div>
                                        </div>
                                        <div className="form-group row mb-4">
                                            <label for="horizontal-firstname-input" className="col-sm-3 col-form-label">Country<span class="m-l-5 text-red">*</span></label>
                                            <div className="col-md-7 col-sm-12">
                                            <select name="country" className="form-control" onChange={(e) => setCountry(e.target.value)}>
                                                {
                                                    countries.map((data) => (
                                                        <option key={data._id} value={ data.countryName }> { data.countryName } </option>
                                                    ))
                                                }
                                            </select> 
                                            </div>
                                        </div>
                                        <div className="form-group row mb-4">
                                            <label for="horizontal-firstname-input" className="col-sm-3 col-form-label">Zip Code<span class="m-l-5 text-red">*</span></label>
                                            <div className="col-md-7 col-sm-12">
                                                <input type="text" className="form-control" id="horizontal-firstname-input" value={zipcode} onChange={(e) => setZipcode(e.target.value)} />
                                            </div>
                                        </div>
                                        <div className="form-group row mb-4">
                                            <label for="horizontal-firstname-input" className="col-sm-3 col-form-label">Area<span class="m-l-5 text-red">*</span></label>
                                            <div className="col-md-7 col-sm-12">
                                                <input type="text" className="form-control" id="horizontal-firstname-input" value={area} onChange={(e) => setArea(e.target.value)} />
                                            </div>
                                        </div>
                                        <div className="form-group row mb-4">
                                            <label for="horizontal-firstname-input" className="col-sm-3 col-form-label">Customer Type<span class="m-l-5 text-red">*</span></label>
                                            <div className="col-md-7 col-sm-12">
                                                <select name="country" className="form-control" onChange={(e) => setCustomerType(e.target.value)}>
                                                    <option value="Normal">Normal</option>
                                                    <option value="Gold">Gold</option>
                                                    <option value="Diamond">Diamond</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div className="form-group row mb-4">
                                            <label for="horizontal-password-input" className="col-sm-3 col-form-label">Status</label>
                                            <div className="text-box col-md-7 col-sm-12">
                                                <input type="checkbox" name="category_status" id="category_status" onChange={(e) => setStatus(e.target.value ? true : false)} />
                                                <label for="category_status">Enable the customer?</label>
                                            </div>
                                        </div>

                                        <div className="form-group row justify-content-end">
                                            <label for="horizontal-email-input" className="col-sm-3 col-form-label"></label>
                                            <div className="col-md-9 col-sm-12">
                                                <div>
                                                    <button onClick={createBrand} className="btn btn-primary w-md">Save</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}


export default AddCustomer;