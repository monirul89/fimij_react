import React, { useState, useEffect } from "react";
import { Link, Redirect } from "react-router-dom";
import Swal from 'sweetalert2';
const axios = require('axios');

const AddCoupon = () => {
    const [name, setName] = useState(null);
    const [code, setCode] = useState("");
    const [isPercent, setIsPercent] = useState(0);
    const [value, setValue] = useState(0);
    const [minimumSpend, setMinimumSpend] = useState(0);
    const [maximumSpend, setMaximumSpend] = useState(0);
    const [usageLimitPerCoupon, setUsageLimitPerCoupon] = useState(0);
    const [usageLimitPerCustomer, setUsageLimitPerCustomer] = useState(0);
    const [isActive, setIsActive] = useState(0);

    const createBrand = (e) => {
        e.preventDefault();

        axios.post("http://127.0.0.1:5000/admin/coupon", {
            name: name,
            code: code,
            isPercent: isPercent,
            value: value,
            minimumSpend: minimumSpend,
            maximumSpend: maximumSpend,
            usageLimitPerCoupon: usageLimitPerCoupon,
            usageLimitPerCustomer: usageLimitPerCustomer,
            isActive: isActive,

        })
            .then((response) => {
                setName("");
                setCode("");
                setIsPercent(false);
                setValue(0);
                setMinimumSpend(0);
                setMaximumSpend(0);
                setUsageLimitPerCoupon(0);
                setUsageLimitPerCustomer(0);
                setIsActive(true);

                Swal.fire({
                    icon: 'success',
                    title: "Coupon successfully created",
                    showConfirmButton: false,
                    timer: 3000
                })
            }).catch((err) => {
            Swal.fire({
                icon: 'error',
                title: err.message,
                showConfirmButton: false,
                timer: 3000
            })
        });
    }

    return (
        <div className="main-content">
            <div className="page-content">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-12">
                            <div className="page-title-box d-flex align-items-center justify-content-between">
                                <h4 className="mb-0 font-size-18">Add Coupon</h4>
                                <div className="page-title-right">
                                    <ol className="breadcrumb m-0">
                                        <li className="breadcrumb-item"><a href="javascript: void(0);">Add Coupon</a></li>
                                        <li className="breadcrumb-item active">Add Coupon</li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-9 categories_body">
                            <div className="category_option card_option">
                                <div className="card">

                                    <div className="card-body">
                                        <div className="form-group row mb-4">
                                            <label for="horizontal-firstname-input" className="col-sm-3 col-form-label"> Name<span class="m-l-5 text-red">*</span></label>
                                            <div className="col-md-7 col-sm-12">
                                                <input type="text" className="form-control" id="horizontal-firstname-input" value={name} onChange={(e) => setName(e.target.value)} />
                                            </div>
                                        </div>
                                        <div className="form-group row mb-4">
                                            <label for="horizontal-firstname-input" className="col-sm-3 col-form-label"> Code<span class="m-l-5 text-red">*</span></label>
                                            <div className="col-md-7 col-sm-12">
                                                <input type="text" className="form-control" id="horizontal-firstname-input" value={code} onChange={(e) => setCode(e.target.value)} />
                                            </div>
                                        </div>

                                        <div className="form-group row mb-4">
                                            <label htmlFor="horizontal-password-input"
                                                   className="col-sm-3 col-form-label">Is Percent</label>
                                            <div className="text-box col-md-7 col-sm-12">
                                                <input type="checkbox" name="category_status" id="category_status"
                                                       onChange={(e) => setIsPercent(e.target.value ? true : false)}/>
                                                <label htmlFor="category_status">Is Percent?</label>
                                            </div>
                                        </div>
                                        <div className="form-group row mb-4">
                                            <label htmlFor="horizontal-firstname-input"
                                                   className="col-sm-3 col-form-label"> Value<span
                                                className="m-l-5 text-red"> </span></label>
                                            <div className="col-md-7 col-sm-12">
                                                <input type="text" className="form-control"
                                                       id="horizontal-firstname-input" value={value}
                                                       onChange={(e) => setValue(e.target.value)}/>
                                            </div>
                                        </div>
                                        <div className="form-group row mb-4">
                                            <label htmlFor="horizontal-firstname-input"
                                                   className="col-sm-3 col-form-label"> Minimum Spend<span
                                                className="m-l-5 text-red"> </span></label>
                                            <div className="col-md-7 col-sm-12">
                                                <input type="text" className="form-control"
                                                       id="horizontal-firstname-input" value={minimumSpend}
                                                       onChange={(e) => setMinimumSpend(e.target.value)}/>
                                            </div>
                                        </div>
                                        <div className="form-group row mb-4">
                                            <label htmlFor="horizontal-firstname-input"
                                                   className="col-sm-3 col-form-label"> Maximum Spend<span
                                                className="m-l-5 text-red"> </span></label>
                                            <div className="col-md-7 col-sm-12">
                                                <input type="text" className="form-control"
                                                       id="horizontal-firstname-input" value={maximumSpend}
                                                       onChange={(e) => setMaximumSpend(e.target.value)}/>
                                            </div>
                                        </div>
                                        <div className="form-group row mb-4">
                                            <label htmlFor="horizontal-firstname-input"
                                                   className="col-sm-3 col-form-label"> Usage Limit Per Coupon<span
                                                className="m-l-5 text-red"> </span></label>
                                            <div className="col-md-7 col-sm-12">
                                                <input type="text" className="form-control"
                                                       id="horizontal-firstname-input" value={usageLimitPerCoupon}
                                                       onChange={(e) => setUsageLimitPerCoupon(e.target.value)}/>
                                            </div>
                                        </div>
                                        <div className="form-group row mb-4">
                                            <label htmlFor="horizontal-firstname-input"
                                                   className="col-sm-3 col-form-label"> Usage Limit Per Customer<span
                                                className="m-l-5 text-red"> </span></label>
                                            <div className="col-md-7 col-sm-12">
                                                <input type="text" className="form-control"
                                                       id="horizontal-firstname-input" value={usageLimitPerCustomer}
                                                       onChange={(e) => setUsageLimitPerCustomer(e.target.value)}/>
                                            </div>
                                        </div>
                                        <div className="form-group row mb-4">
                                            <label htmlFor="horizontal-password-input"
                                                   className="col-sm-3 col-form-label">Is Active</label>
                                            <div className="text-box col-md-7 col-sm-12">
                                                <input type="checkbox" name="category_status" id="category_status"
                                                       onChange={(e) => setIsActive(e.target.value ? true : false)}/>
                                                <label htmlFor="category_status">Is Active?</label>
                                            </div>
                                        </div>



                                        <div className="form-group row justify-content-end">
                                            <label for="horizontal-email-input" className="col-sm-3 col-form-label"></label>
                                            <div className="col-md-9 col-sm-12">
                                                <div>
                                                    <button onClick={createBrand} className="btn btn-primary w-md">Save</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}


export default AddCoupon;