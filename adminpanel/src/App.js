import React, { lazy, Suspense } from "react";
import { BrowserRouter as Router, Switch, Route, Link, Redirect } from "react-router-dom";
//style 
import '../public/assets/css/bootstrap.min.css';
import '../public/assets/css/icons.min.css';
import '../public/assets/css/app.min.css';

//Pages Imports
import Dashboard from './pages/Dashboard.js';
import AddToCart from './pages/AddToCart.js';
import Login from './pages/Login';
import Register from './pages/Signup';
import AddCategory from './components/Category/AddCategory';
import CategoryList from './components/Category/CategoryList';
import AddBrand from './components/brand/AddBrand';
import BrandList from './components/brand/BrandList';
import CountryList from './components/Country/CountryList';
import AddCountry from './components/Country/AddCountry';
import Sales from './pages/sales/Sales';
import Blog from './pages/blog/Blog';
import ProductList from './components/Product/ProductList'
import AddProduct from './components/Product/AddProduct'
import OrderList from './components/Orders/OrderList'
import AddSalesOrder from './components/Orders/AddSalesOrder'
import CustomerList from './components/Customer/CustomerList'
import AddCustomer from './components/Customer/AddCustomer'
import CouponList from './components/Coupon/CouponList'
import AddCoupon from './components/Coupon/AddCoupon'
import Notfound from './pages/notfound/Notfound'

//Component Imports
import Header from './components/Header.js';
import Footer from './components/Footer.js';

export default class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpen: true
        };
    }
	render() {
		const { isOpen } = this.state

		let currenturl = window.location.href;
		console.log(currenturl);
		if(currenturl=='http://localhost:3000/signin'){
			return (
				<div>
					<Login />
				</div>
			)
		}else if(currenturl=='http://localhost:3000/signup'){
			return (
				<div>
					<Register />
				</div>
			)
		}else{
			return (
				<div>
					<Suspense fallback={<div className="full-page-loader">Loding Text...</div>}>
						<Router>
							<div>
								<Header />
								<Switch>
									<Route path="/admin" component={Dashboard} /> 
									<Route path="/add-to-cart" component={AddToCart} /> 
									<Route path="/signin" component={Login} /> 
									<Route path="/signup" component={Register} /> 
									<Route path="/category/list" component={CategoryList} /> 
									<Route path="/category/add" component={AddCategory} />
									<Route path='/category/edit/:id' component={AddCategory}/>
									<Route path="/brand/list" component={BrandList} /> 
									<Route path="/brand/add" component={AddBrand} /> 
									<Route path="/country/list" component={CountryList} /> 
									<Route path="/country/add" component={AddCountry} /> 
									<Route path="/sales" component={Sales} /> 
									<Route path="/blogs" component={Blog} /> 
									<Route path="/blog/add" component={Blog} /> 
									<Route path="/products/list" component={ProductList} /> 
									<Route path="/products/add" component={AddProduct} /> 
									<Route path="/orders/add" component={AddSalesOrder} />
									<Route path="/orders/list" component={OrderList} />
									<Route path="/coupon/list" component={CouponList} />
									<Route path="/coupon/add" component={AddCoupon} />
									<Route path="/customer/list" component={CustomerList} />
									<Route path="/customer/add" component={AddCustomer} />
									<Route path="*" component={Notfound} />
								</Switch>
								<Footer />
							</div>
						</Router>
					</Suspense>
				</div>
			)
		}

	}
}
